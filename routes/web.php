<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/tuyen-dung', 'RecruitmentController@index')->name('recruitment');
Route::get('/lien-he', 'ContactController@index')->name('contact');
Route::get('/gioi-thieu', 'AboutController@index')->name('about');
Route::get('/tin-tuc', 'NewController@index')->name('new');
Route::get('/dao-tao', 'EducationController@index')->name('education');
Route::get('/su-kien', 'EventController@index')->name('event');
Route::get('/hinh-anh-su-kien', 'GalleryController@index')->name('gallery');
Route::get('/hinh-anh/{slug}', 'GalleryController@detail')->name('gallery-detail');
//Route::get('/ve-dat-nuoc-canada', 'ServiceController@index')->name('service');
Route::get('/chi-tiet-su-kien/{id}', 'EventController@detail')->name('event-detail');
Route::get('/chi-tiet-tin-tuc/{id}', 'NewController@detail')->name('new-detail');
Route::get('/chi-tiet-dao-tao/{id}', 'EducationController@detail')->name('education-detail');
Route::get('/dang-ky', 'RegisterController@index')->name('register');
Route::get('/dinh-cu/{slug}', 'SettlementController@detail')->name('settlement');
Route::post('/dinh-cu', 'SettlementController@compare')->name('get-settlement');
Route::get('/xuc-tien-thuong-mai-dau-tu', 'TradeInvestmentController@index')->name('trade-investment');
Route::get('/dau-tu/{slug}', 'InvestmentController@index')->name('investment');
Route::get('/chi-tiet-dau-tu/{slug}', 'InvestmentController@detail')->name('investment-detail');
Route::post('/dang-ky', 'RegisterController@store')->name('new-customer');
Route::post('/lien-he', 'ContactController@store')->name('new-contact');
Route::post('/export-csv', 'RegisterController@export_csv')->name('export_csv');
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
