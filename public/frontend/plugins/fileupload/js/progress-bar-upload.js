$(function () {

  $("#avatar").fileupload({
    dataType: 'json',
    sequentialUploads: true,
    maxChunkSize: 2000000,

    start: function (e) {
      $(".ps-step-form .progress").removeClass('hidden');
    },

    stop: function (e) {
      setTimeout(function(){ $(".ps-step-form .progress").addClass('hidden');$("#upload-success").removeClass('active'); }, 3000);
    },

    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      var strProgress = progress + "%";
      $(".ps-step-form .progress-bar").css({"width": strProgress});
      $(".ps-step-form .progress-bar").text(strProgress);
    },

    done: function (e, data) {
      if (data.result.status == true) {
        $(".avatar-img").attr('src', data.result.url);
        $("#upload-success").addClass('active');
      }
    }

  });

  $("#file_gpkd").fileupload({
    dataType: 'json',
    sequentialUploads: true,
    maxChunkSize: 20000000,

    start: function (e) {
      $(".ps-step-form .progress").removeClass('hidden');
    },

    stop: function (e) {
      setTimeout(function(){ $(".ps-step-form .progress").addClass('hidden');$("#upload-success").removeClass('active'); }, 3000);
    },

    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      var strProgress = progress + "%";
      $(".ps-step-form .progress-bar").css({"width": strProgress});
      $(".ps-step-form .progress-bar").text(strProgress);
      
      if(progress>=100){
        document.location.reload();
      }
    },

    done: function (e, data) {
      if (data.result.status == true) {
        // $("a.file_gpkd").attr('href', data.result.url);
        $("input.file_gpkd").val(data.result.file);
        $("a.file_gpkd").removeClass('hidden');
        $("#upload-success").addClass('active');
        
      }
    }

  });

  $("#file_don_dk").fileupload({
    dataType: 'json',
    sequentialUploads: true,
    maxChunkSize: 20000000,

    start: function (e) {
      $(".ps-block--register-offline .progress").removeClass('hidden');
    },

    stop: function (e) {
      setTimeout(function(){ $(".ps-block--register-offline .progress").addClass('hidden');$("#upload-success").removeClass('active'); }, 3000);
    },

    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      var strProgress = progress + "%";
      $(".ps-block--register-offline .progress-bar").css({"width": strProgress});
      $(".ps-block--register-offline .progress-bar").text(strProgress);
    },

    done: function (e, data) {
      if (data.result.status == true) {
        $("a.file_don_dk").attr('href', data.result.url);
        $("a.file_don_dk").removeClass('hidden');
        $("#upload-success").addClass('active');
      }
    }

  });

  $("#file_quyen_shct").fileupload({
    dataType: 'json',
    sequentialUploads: true,
    maxChunkSize: 20000000,

    start: function (e) {
      $(".ps-step-form .progress").removeClass('hidden');
    },

    stop: function (e) {
      setTimeout(function(){ 
        $(".ps-step-form .progress").addClass('hidden'); 
        $("#upload-success").removeClass('active');
      }, 3000);
    },

    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      var strProgress = progress + "%";
      $(".ps-step-form .progress-bar").css({"width": strProgress});
      $(".ps-step-form .progress-bar").text(strProgress);
    },

    done: function (e, data) {
      if (data.result.status == true) {
        $("a.file_quyen_shct").attr('href', data.result.url);
        $("a.file_quyen_shct").removeClass('hidden');
        $("#upload-success").addClass('active');
      }
    }

  });

});