@extends('layouts.app')

@section('page_title')
   {{ __('message.gallery_event') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
<?php
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="gallery-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' => __('message.gallery_event')])
        <div class="large-10 large-offset-1 small-12 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' => __('message.Gallery'),
                    'url' => ''
                ]
            ]])
            <div class="grid-x body">
                <div class="large-4 medium-4 small-8 grid-x tab-header">
                    <div id="t1" class="tab-title" onclick="openTab(event, 'gal1')">{{ __('message.Gallery') }}</div>
                    <div id="t2" class="tab-title" onclick="openTab(event, 'gal2')">Video</div>
                </div>
                <div class="small-12 tab-content" id="gal1">
                    @if(count($groups) > 0)
                        <div class="event-list">
                        @foreach($groups as $new)
                                <div class="small-12 grid-x event-item">
                                    <div class="small-12 medium-2 image">
                                        <a href="{{ route('gallery-detail', $new->slug) }}">
                                            <img src="{{ asset('frontend/img/gallery.png') }}" />
                                        </a>
                                    </div>

                                    <div class="small-12 medium-10 info">

                                        <a href="{{ route('gallery-detail', $new->slug) }}">
                                            <div class="title gallery-title">
                                                @if ($locale == 'en' && $new->title_en)
                                                    {{ $new->title_en }}
                                                @else
                                                    {{ $new->title }}
                                                @endif
                                            </div>
                                        </a>

                                        <div class="time-container">
                                            <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                            <span>{{ date("d/m/Y h:m a",strtotime($new->created_at)) }}</span>
                                        </div>

                                    </div>
                                </div>

                        @endforeach
                    </div>
                        {{ $groups->appends(array('tab' => 'gal1',))->links() }}
                    @else
                        <div class="no-data">
                            {{ __('message.updating') }}
                    </div>
                    @endif
                </div>
                <div class="small-12 tab-content" id="gal2">
                    @if(count($videos) > 0)
                        <div class="event-list">
                        @foreach($videos as $video)
                            <div class="small-12 grid-x event-item e-video" link="https://www.youtube.com/embed/{{ $video->video_id }}">
                                <div class="small-12 medium-3 image">
                                    <img src="https://i.ytimg.com/vi/{{ $video->video_id  }}/hqdefault.jpg" />
                                </div>
                                <div class="small-12 medium-9 info">
                                    <div class="title">

                                        @if ($locale == 'en' && $video->title_en)
                                            {{ $video->title_en }}
                                        @else
                                            {{ $video->title }}
                                        @endif
                                    </div>
                                    <div class="time-container">
                                        <i class="fa fa-play" aria-hidden="true"></i> <span>{{ $video->time }}</span>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                        {{ $videos->appends(array('tab' => 'gal2',))->links() }}
                    @else
                        <div class="no-data">
                           {{ __('message.updating') }}
                        </div>
                    @endif
                </div>
                <div class="small-12 viewed-new-container">
                    <div class="title">{{ __('message.related_article') }}</div>
                    <div class="carousel-wrap">
                        <div class="owl-carousel" id="viewed-new-carousel">
                            @foreach($news as $n)
                                <a href="{{ route('new-detail', $n->slug ? $n->slug : $n->id) }}">
                                    <div class="item">
                                        <img src="{{ Voyager::image($n->image) }}">
                                        <div class="info">
                                            <div class="category">
                                                @if($n->category)
                                                    <div class="category">
                                                        @if ($locale == 'en' &&  $n->category->name_en)
                                                            {{ $n->category->name_en }}
                                                        @else
                                                            {{  $n->category->name }}
                                                        @endif
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="title">
                                                @if ($locale == 'en' &&  $n->title_en)
                                                    {{ $n->title_en }}
                                                @else
                                                    {{ $n->title }}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">


                    <div class="modal-body">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
                        </div>


                    </div>

                </div>
            </div>
        </div>
        @foreach($events as $event)
            <div style="display: none;" id="event-{{ $event->id }}">
                @foreach($event->gallery as $g)
                    <?php $arrEventImage = json_decode($g->image) ?>
                    @foreach($arrEventImage as $item)
                            <a class="gal" href="{{ Voyager::image($item) }}">
                                <img src="{{ Voyager::image($item) }}" />
                            </a>
                    @endforeach

                @endforeach
            </div>
        @endforeach
        @foreach($services as $service)
            <div style="display: none;" id="service-{{ $service->id }}">
                @foreach($service->gallery as $g)
                    <?php $arrServiceImage = json_decode($g->image); ?>
                    @foreach($arrServiceImage as $item)
                            <a class="gal" href="{{ Voyager::image($item) }}">
                                <img src="{{ Voyager::image($item) }}" />
                            </a>
                    @endforeach
                @endforeach
            </div>
        @endforeach
    </div>
@stop
@section('javascript')
    <script>
      function openTab(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].className = tabcontent[i].className.replace(" active", "");
        }
        tablinks = document.getElementsByClassName("tab-title");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).className += " active";
        evt.currentTarget.className += " active";
      }

      $(document).ready(function(){
          @foreach($services as $service)
                  $('#service-{{ $service->id }}').lightGallery();
          @endforeach
          @foreach($events as $event)
            $('#event-{{ $event->id }}').lightGallery();
          @endforeach
//        $('.gallery-title').click(function() {
//          var id =$(this).attr('data-id');
//          $('#'+id + ' .gal').click()
//        });
//        $('.image img').click(function() {
//          var id =$(this).attr('data-id');
//          $('#'+id + ' .gal').click()
//        });
        var stringSearch = window.location.search;
        $('.tab-content').removeClass('active');
        $('.tab-title').removeClass('active');
        if (stringSearch.indexOf('tab=gal1') !== -1) {
          $('#t1').addClass('active');
          $('#gal1').addClass('active')
        } else if(stringSearch.indexOf('tab=gal2') !== -1) {
          $('#t2').addClass('active');
          $('#gal2').addClass('active')
        } else {
          $('#t1').addClass('active');
          $('#gal1').addClass('active');
        }
        $('#viewed-new-carousel').owlCarousel({
          loop:true,
          mouseDrag: false,
          touchDrag: true,
          margin: 15,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          responsive:{
            0:{
              items:1,
              nav:true
            },
            600:{
              items:3,
              nav:true
            },
            1000:{
              items:4,
              nav:true,
              loop:true
            }
          }
        });
        $('.e-video').click(function() {
          $('#myModal').modal();
          var link = $(this).attr('link');
          $("#video").attr('src',link + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" );
        });
        $('#myModal').on('hide.bs.modal', function (e) {
          // a poor man's stop video
          $("#video").attr('src', '');
        })
      })
    </script>
@stop
