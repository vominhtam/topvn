@extends('layouts.app')

@section('page_title')
    {{ __('message.TI') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
<?php
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="new-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' => __('message.News') ])
        <div class="small-12 large-10 large-offset-1 grid-content new-body">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' => __('message.TI'),
                    'url' => ''
                ]
            ]])
            <div class="grid-x body">
                <div class="small-12 grid-x tab-header">
                    <div id="t1" class="tab-title" onclick="openTab(event, 'new1')">{{ __('message.all') }}</div>
                    <div id="t3" class="tab-title" onclick="openTab(event, 'new3')">{{ __('message.trade_investment') }}</div>
                    <div id="t2" class="tab-title" onclick="openTab(event, 'new2')">{{ __('message.trade') }}</div>
                </div>
                <div class="small-12 tab-content" id="new1">
                    @if(count($news) > 0)
                        <div class="event-list">
                        @foreach($news as $new)
                            <div class="small-12 grid-x event-item">
                                <div class="small-12 medium-3 image">
                                    <a href="{{ route('new-detail', $new->slug ? $new->slug : $new->id) }}">
                                        <img src=" {{ Voyager::image($new->image) }}" />
                                    </a>

                                </div>
                                <div class="small-12 medium-9 info">
                                    @if($new->category)
                                        <div class="category">
                                            @if ($locale == 'en' &&  $new->category->name_en)
                                                {{ $new->category->name_en }}
                                            @else
                                                {{  $new->category->name }}
                                            @endif
                                        </div>
                                    @endif
                                    <a href="{{ route('new-detail', $new->slug ? $new->slug : $new->id) }}">
                                        <div class="title">
                                            @if ($locale == 'en' &&  $new->title_en)
                                                {{ $new->title_en }}
                                            @else
                                                {{ $new->title }}
                                            @endif

                                        </div>
                                    </a>

                                    <div class="time-container">
                                        <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                        <span>{{ date("d/m/Y h:m a",strtotime($news[0]->created_at)) }}</span>
                                    </div>
                                    <div class="description-container">
                                        <div class="description">
                                            @if ($locale == 'en' &&  $new->description_en)
                                                {{ $new->description_en }}
                                            @else
                                                {{ $new->description }}
                                            @endif
                                        </div>
                                        @if($new->description)
                                            <a href="{{ route('new-detail', $new->slug ? $new->slug : $new->id) }}"><span class="read-more">{{ __('message.read_more') }}</span></a>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                        {{ $news->appends(array('tab' => 'new1',))->links() }}
                    @else
                        <div class="no-data">
                           {{ __('message.updating') }}
                        </div>
                    @endif
                </div>
                <div class="small-12 tab-content" id="new2">
                    @if(count($news1) > 0)
                        <div class="event-list">
                        @foreach($news1 as $new)
                            <div class="small-12 grid-x event-item">
                                <div class="small-12 medium-3 image">
                                    <a href="{{ route('new-detail', $new->slug ? $new->slug : $new->id) }}">
                                        <img src=" {{ Voyager::image($new->image) }}" />
                                    </a>
                                </div>
                                <div class="small-12 medium-9 info">
                                    @if($new->category)
                                        <div class="category">
                                            @if ($locale == 'en' &&  $new->category->name_en)
                                                {{ $new->category->name_en }}
                                            @else
                                                {{  $new->category->name }}
                                            @endif
                                        </div>
                                    @endif
                                    <a href="{{ route('new-detail', $new->slug ? $new->slug : $new->id) }}">
                                        <div class="title">
                                            @if ($locale == 'en' &&  $new->title_en)
                                                {{ $new->title_en }}
                                            @else
                                                {{ $new->title }}
                                            @endif
                                        </div>
                                    </a>
                                    <div class="time-container">
                                        <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                        <span>{{ date("d/m/Y h:m a",strtotime($news[0]->created_at)) }}</span>
                                    </div>
                                    <div class="description-container">
                                        <div class="description">
                                            @if ($locale == 'en' &&  $new->description_en)
                                                {{ $new->description_en }}
                                            @else
                                                {{ $new->description }}
                                            @endif

                                        </div>
                                        @if($new->description)
                                            <a><span class="read-more">{{ __('message.read_more') }}</span></a>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                        {{ $news1->appends(array('tab' => 'new2',))->links() }}
                    @else
                        <div class="no-data">
                            {{ __('message.updating') }}
                        </div>
                    @endif
                </div>
                <div class="small-12 tab-content" id="new3">
                    @if(count($news2) > 0)
                        <div class="event-list">
                        @foreach($news2 as $new)
                            <div class="small-12 grid-x event-item">
                                <div class="small-12 medium-3 image">
                                    <img src=" {{ Voyager::image($new->image) }}" />
                                </div>
                                <div class="small-12 medium-9 info">
                                    @if($new->category)
                                        @if ($locale == 'en' &&  $new->category->name_en)
                                            {{ $new->category->name_en }}
                                        @else
                                            {{  $new->category->name }}
                                        @endif
                                    @endif
                                    <a href="{{ route('new-detail', $new->slug ? $new->slug : $new->id) }}">
                                        <div class="title">
                                            @if ($locale == 'en' &&  $new->title_en)
                                                {{ $new->title_en }}
                                            @else
                                                {{ $new->title }}
                                            @endif
                                        </div>
                                    </a>
                                    <div class="time-container">
                                        <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                        <span>{{ date("d/m/Y h:m a",strtotime($news[0]->created_at)) }}</span>
                                    </div>
                                    <div class="description-container">
                                        <div class="description">
                                            @if ($locale == 'en' &&  $new->description_en)
                                                {{ $new->description_en }}
                                            @else
                                                {{ $new->description }}
                                            @endif

                                        </div>
                                        @if($new->description)
                                            <a><span class="read-more">{{ __('message.read_more') }}</span></a>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                        {{ $news2->appends(array('tab' => 'new3',))->links() }}
                    @else
                        <div class="no-data">
                            {{ __('message.updating') }}
                        </div>
                    @endif
                </div>
                <div class="small-12 viewed-new-container grid-x">
                    <div class="title small-12">{{ __('message.related_article') }}</div>
                    @if(count($news3) > 0)
                        <div class="carousel-wrap small-12">
                        <div class="owl-carousel" id="viewed-new-carousel">
                            @foreach($news3 as $n)
                                <div class="item">
                                    <img src="{{ Voyager::image($n->image) }}">
                                    <div class="info">
                                        @if($n->category)
                                            <div class="category">
                                                @if ($locale == 'en' &&  $n->category->name_en)
                                                    {{ $n->category->name_en }}
                                                @else
                                                    {{  $n->category->name }}
                                                @endif
                                            </div>
                                        @endif
                                        <a href="{{ route('new-detail', $n->slug ? $n->slug : $n->id) }}">
                                            <div class="title">
                                                @if ($locale == 'en' &&  $n->title_en)
                                                    {{ $n->title_en }}
                                                @else
                                                    {{ $n->title }}
                                                @endif
                                            </div>
                                        </a>


                                    </div>
                                </div>


                            @endforeach
                        </div>
                    </div>
                    @else
                        <div class="no-data">
                           {{ __('message.updating') }}
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
@stop
@section('javascript')
    <script>
      function openTab(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tab-content");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].className = tabcontent[i].className.replace(" active", "");
        }
        tablinks = document.getElementsByClassName("tab-title");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).className += " active";
        evt.currentTarget.className += " active";
      }

      $(document).ready(function(){
        var stringSearch = window.location.search;
        $('.tab-content').removeClass('active');
        $('.tab-title').removeClass('active');
        if (stringSearch.indexOf('tab=new1') !== -1) {
          $('#t1').addClass('active');
          $('#new1').addClass('active')
        } else if(stringSearch.indexOf('tab=new2') !== -1) {
          $('#t2').addClass('active');
          $('#new2').addClass('active')
        } else if(stringSearch.indexOf('tab=new3') !== -1) {
          $('#t3').addClass('active');
          $('#new3').addClass('active');
        } else {
          $('#t1').addClass('active');
          $('#new1').addClass('active');
        }
        $('#viewed-new-carousel').owlCarousel({
          loop:true,
          mouseDrag: false,
          touchDrag: true,
          margin: 15,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          responsive:{
            0:{
              items: 1,
              nav:true
            },
            600:{
              items: 3,
              nav:true
            },
            1000:{
              items:4,
              nav:true,
              loop:true
            }
          }
        });
        $('#change-page').click(function() {
          var url = window.location.href;
          if (url.indexOf('?') === -1){
            url += '?category=' + $('#select-category').val();

          } else if (url.indexOf('category')) {
            var query_string = url.search;
            var search_params = new URLSearchParams(query_string);
            search_params.set('category', $('#select-category').val());
            url = new URL(url);
            url.search = search_params.toString();
            url = url.toString();
          } else {
            url += '&category=' + $('#select-category').val();
          }
          window.location.href = url;
        })
      })
    </script>
@stop
