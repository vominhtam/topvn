@extends('layouts.app')

@section('page_title')
    {{ __('message.Register') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
<?php
$avatarDesktop = Voyager::setting('site.avatar_desktop', '');
$avatarMobile = Voyager::setting('site.avatar_mobile', '');
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="home-page">
        @include('layouts.banner', ['banner' => $banner, 'title' => __('message.home_sign_up')])
        <div class="grid-x register-form">
            <div class="small-12 large-10 large-offset-1 grid-content desktop">
                <div class="grid-x body">
                    <div class="small-12 medium-6 form-controller">
                        <form>
                            <div class="small-12 medium-6 form-controller" style="padding-top: 0;">
                                <div class="text-title small-12 global-title">
                                    {{ __('message.home_sign_up') }}
                                </div>
                                <div class="small-12 medium-12 item">
                                    <input id="fullname" placeholder="{{ __('message.fullname') }}" name="fullname" required />
                                </div>
                                <div class="small-12 item">
                                    <input id="phone" placeholder="{{ __('message.phone') }}" name="phone" required />
                                </div>
                                <div class="small-12 item">
                                    <input id="email" type="email" placeholder="Email" name="email" required />
                                </div>
                                <div class="small-12 item">
                                    <select id="category" name="category" class="home-select">
                                        @foreach($categories as $cat)
                                            <option value="{{ $cat->id }}">
                                                @if ($locale == 'en' && $cat->name_en)
                                                    {{ $cat->name_en }}
                                                @else
                                                    {{ $cat->name }}
                                                @endif
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="small-12 item">
                                    <button id="register" data-target="#event-modal">{{ __('message.send_information') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="small-12 medium-6 image-controller">
                        <img class="image desktop" src="{{ Voyager::image($avatarDesktop) }}">
                        <img class="image mobile" src="{{ Voyager::image($avatarMobile) }}">
                    </div>
                    {{--<div class="large-3">--}}
                        {{--<img class="image" src="{{ asset('frontend/img/girl.png') }}">--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="small-12 large-10 large-offset-1 grid-content mobile register-page">
                <div class="grid-x body">
                    <form>
                        <div class="small-12 form-controller">
                            <div class="text-title small-12 global-title">
                                {{ __('message.home_sign_up') }}
                            </div>
                            <div class="small-12 medium-12 item">
                                <input id="fullname-mobile" placeholder="{{ __('message.fullname') }}" name="fullname" required />
                            </div>
                            <div class="small-12 item">
                                <input id="phone-mobile" placeholder="{{ __('message.phone') }}" name="phone" required />
                            </div>
                            <div class="small-12 item">
                                <input id="email-mobile" placeholder="Email" name="email" required />
                            </div>
                            <div class="small-12 item">
                                <select id="category-mobile" name="category">
                                    @foreach($categories as $cat)
                                        <option value="{{ $cat->id }}">
                                            @if ($locale == 'en' && $cat->name_en)
                                                {{ $cat->name_en }}
                                            @else
                                                {{ $cat->name }}
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        {{--<img class="image mobile" src="{{ asset('frontend/img/girl-mobile.png') }}">--}}
                        <div class="small-12 item">
                            <button id="register-mobile" data-target="#event-modal">{{ __('message.send_information') }}</button>
                        </div>
                    </form>

                    {{--<div class="large-3">--}}
                    {{--<img class="image" src="{{ asset('frontend/img/girl.png') }}">--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        <div class="modal fade" id="event-modal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div id="modal-succeed">
                        <div class="modal-header">
                            <div class="header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body grid-x">
                                <div class="small-12">
                                    <img class="icon" src="{{ asset('frontend/img/succeed-icon.svg') }}" />
                                </div>
                                <div class="info">
                                    <div class="title">{{ Voyager::setting('site.modal_title') }}</div>
                                    <div class="description">
                                        {{ Voyager::setting('site.modal_message') }}
                                    </div>
                                    <button class="btn-sumit" data-dismiss="modal" id="btn-back">
                                       {{ __('message.back_to_home') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript">
      $(document).ready(function() {
        function checkEmail(mail)
        {
          if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
          {
            return true;
          }
          return false;
        }
        $('#register').click(function(e) {
          e.preventDefault();
          var fullname = $('#fullname').val();
          var phone = $('#phone').val();
          var email = $('#email').val();
          var category = $('#category').val();
          if (!fullname) {
            alert('Vui lòng nhập họ và tên !');
            return false;
          }
          if (!phone) {
            alert('Vui lòng nhập số điện thoại !');
            return false;
          }
          if (!email) {
            alert('Vui lòng nhập email !');
            return false;
          }

          if (isNaN(phone) || phone.length !== 10) {
            alert('Số điện thoại không hợp lệ');
            return false;
          }
          if (!checkEmail(email)) {
            alert('Email không hợp lệ');
            return false;
          }
          $.ajaxSetup({

            headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
          });
          $.ajax({

            type:'POST',

            url:'{!! route('new-customer') !!}',

            data:{phone: phone, fullname: fullname, email: email, category: category},

            success:function(data){

              $('#event-modal').modal();
              $('#fullname').val('');
              $('#phone').val('');
              $('#email').val('');

            }

          });

        });
        $('#register-mobile').click(function(e) {
          e.preventDefault();
          var fullname = $('#fullname-mobile').val();
          var phone = $('#phone-mobile').val();
          var email = $('#email-mobile').val();
          var category = $('#category-mobile').val();
          if (!fullname) {
            alert('Vui lòng nhập họ và tên !');
            return false;
          }
          if (!phone) {
            alert('Vui lòng nhập số điện thoại !');
            return false;
          }
          if (!email) {
            alert('Vui lòng nhập email !');
            return false;
          }

          if (isNaN(phone) || phone.length !== 10) {
            alert('Số điện thoại không hợp lệ');
            return false;
          }
          if (!checkEmail(email)) {
            alert('Email không hợp lệ');
            return false;
          }
          $.ajaxSetup({

            headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
          });
          $.ajax({

            type:'POST',

            url:'{!! route('new-customer') !!}',

            data:{phone: phone, fullname: fullname, email: email, category: category},

            success:function(data){

              $('#event-modal').modal();
              $('#fullname-mobile').val('');
              $('#phone-mobile').val('');
              $('#email-mobile').val('');

            }

          });

        });
      });
    </script>
@stop
