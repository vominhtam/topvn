@extends('layouts.app')

@section('page_title')
    {{ __('message.home') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('url')
    {{ setting('site.description') }}
@stop

<?php
$avatarDesktop = Voyager::setting('site.avatar_desktop', '');
$avatarMobile = Voyager::setting('site.avatar_mobile', '');
$locale = \Session::get('locale');
?>

@section('content')
    @include('layouts.header', ['isHome' => true, 'categories' => $categories])
    <div class="home-page">
        <div class="banner">
            <span id="sub-nav-toggle" class="navbar-toggler-icon"> <img src="{{ asset('frontend/img/menu-icon.svg') }}" /></span>
            <div class="owl-carousel" id="banner-carousel">
                @foreach($banner as $b)
                    <div class="item service-item">
                        <img src="{{ Voyager::image($b->image) }}">
                    </div>
                @endforeach
            </div>

        </div>
        <div class="grid-x event-filter-controller">
            <div class="small-7 select-container">
                <select id="select-category">
                    @foreach($categories as $cat)
                        <option value="{{ $cat->slug }}">{{ $cat->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="small-5 button-container">
                <button id="change-page">
                   {{ __('message.search') }}
                </button>
            </div>
        </div>
        <div class="grid-x benefit-container">
            <div class="small-12 large-10 large-offset-1 grid-content grid-x ">
                <div class="small-12 medium-12 large-3 grid-x text-container">
                    <div class="text-title global-title">
                        @if ($locale == 'en')
                            {{ Voyager::setting('site.benefit_title_en') }}
                        @else
                            {{ Voyager::setting('site.benefit_title') }}
                        @endif
                    </div>
                    <div class="text">
                        @if ($locale == 'en')
                            {{ Voyager::setting('site.benefit_description_en') }}
                        @else
                            {{ Voyager::setting('site.benefit_description') }}
                        @endif
                    </div>
                </div>
                <div class="small-12 large-9">
                    <div class="grid-x benefit-body desktop">
                        @foreach($benefits as $benefit)
                            <div class="small-12 medium-4 benefit-item" style="margin-bottom: 20px;">
                                <div class="content">
                                    <img class="logo" src="{{ Voyager::image($benefit->logo) }}"/>
                                    <div class="text bold">
                                        @if ($locale == 'en' && $benefit->name_en)
                                            {{ $benefit->name_en }}
                                         @else
                                            {{ $benefit->name }}
                                         @endif
                                    </div>
                                    <div class="text">
                                        @if ($locale == 'en' && $benefit->name_en)
                                            {{ $benefit->description_en }}
                                        @else
                                            {{ $benefit->description }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="grid-x benefit-body mobile">
                        @foreach($benefits as $benefit)
                            <div class="small-12 benefit-item">
                                <div class="content grid-x">
                                    <div class="small-3">
                                        <img class="logo" src="{{ Voyager::image($benefit->logo) }}"/>
                                    </div>
                                    <div class="small-9">
                                        <div class="text bold">
                                            @if ($locale == 'en' && $benefit->name_en)
                                                {{ $benefit->name_en }}
                                            @else
                                                {{ $benefit->name }}
                                            @endif
                                        </div>
                                        <div class="text">
                                            @if ($locale == 'en' && $benefit->name_en)
                                                {{ $benefit->description_en }}
                                            @else
                                                {{ $benefit->description }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>

        </div>
        <div class="register-container mobile">
            <div class="cover"></div>
            <div class="grid-x flex-center body">
                <div class="content">
                    <div class="text bold">{{ __('message.home_your_mail') }}</div>
                    <div class="text">{{ __('message.home_your_mail_content') }}</div>
                    <div class="input-container">
                        <input id="email-mb" placeholder="{{ __('message.your_mail') }}" />
                        <button id="register-mb" class="btn-send register-rc">
                            {{ __('message.send') }}
                        </button>
                    </div>
                </div>

            </div>
            <img src="{{ asset('frontend/img/register-bg.jpg') }}" />
        </div>
        <div class="service-container grid-x">
            <div class="small-12 large-10 large-offset-1 grid-x grid-content">
                <div class="small-12 text-container">
                    <div class="text-title">
                       {{ __('message.outstanding_service') }}
                    </div>
                </div>
                <div class="carousel-wrap small-12">
                    <div class="owl-carousel" id="service-carousel">
                        @foreach($services as $service)
                            <a href="{{ route('settlement', $service->category->slug) }}">
                                <div class="item service-item">
                                    <img src="{{ Voyager::image($service->image) }}">
                                    <div class="cover">

                                        <span class="text">
                                             @if ($locale == 'en' && $service->name_en)
                                                {{ $service->name_en }}
                                            @else
                                                {{ $service->name }}
                                            @endif
                                        </span>

                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="register-container desktop">
            <div class="cover"></div>
            <div class="grid-x flex-center body">
                <div class="content">
                    <div class="text bold">{{ __('message.home_your_mail') }}</div>
                    <div class="text">{{ __('message.home_your_mail_content') }}</div>
                    <div class="input-container">
                        <input id="email-rc" placeholder="{{ __('message.your_mail') }}" />
                        <button id="register-rc" class="btn-send">
                            {{ __('message.send') }}
                        </button>
                    </div>
                </div>

            </div>
            <img src="{{ asset('frontend/img/register-bg.jpg') }}" />
        </div>
        <div class="grid-x news-container">
            <div class="small-12 large-10 large-offset-1 grid-x grid-content">
                <div class="small-12 text-container">
                    <div class="text-title">
                        {{ __('message.News') }}
                    </div>
                </div>
                <div class="small-12 body grid-x">
                    @if(count($news) > 0)
                        <div class="large-7 left">
                            <a href="{{ route('new-detail', $news[0]->slug ? $news[0]->slug : $news[0]->id) }}">
                                <div class="event-item">
                                    <div class="cover">
                                        <div class="text title">
                                            @if($news[0]->category)
                                                @if ($locale == 'en' && $news[0]->category->name_en)
                                                    {{ $news[0]->category->name_en }}
                                                @else
                                                    {{ $news[0]->category->name }}
                                                @endif
                                            @endif
                                        </div>
                                        <div class="text description">
                                            @if ($locale == 'en' && $news[0]->title_en)
                                                {{ $news[0]->title_en }}
                                            @else
                                                {{ $news[0]->title }}
                                            @endif
                                        </div>
                                        <div class="time-container">

                                            <img src="{{ asset('frontend/img/calendar.svg') }}" />
                                            <span>{{ date("d/m/Y h:m a",strtotime($news[0]->created_at)) }}</span>
                                        </div>
                                    </div>
                                    <img class="background" src="{{ asset('frontend/img/event-3.jpg') }}"/>
                                </div>
                            </a>

                        </div>
                        <div class="small-12 large-5 right">
                            @php
                                $vs = $news->toArray();
                                $vs = array_slice($vs, 1);
                            @endphp
                            @foreach($vs as $new)
                                <a class="link-container" href="{{ route('new-detail', $new['slug'] ? $new['slug'] : $new['id']) }}">
                                    <div class="item grid-x">
                                        <div class="small-12 medium-5 image">
                                            <img src="{{ Voyager::image($new['image']) }}" />
                                        </div>
                                        <div class="info medium-7">
                                            <div class="category">
                                                @if ($locale == 'en' && $new['category']['name_en'])
                                                    {{ $new['category']['name_en'] }}
                                                @else
                                                    {{ $new['category']['name'] }}
                                                @endif
                                            </div>
                                            <div class="title">
                                                @if ($locale == 'en' && $new['title_en'])
                                                    {{ $new['title_en'] }}
                                                @else
                                                    {{ $new['title'] }}
                                                @endif
                                            </div>
                                            <div class="time-container">

                                                <img src="{{ asset('frontend/img/calendar-gray.svg') }}" />
                                                <span>{{ date("d/m/Y h:m a",strtotime($new['created_at'])) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>

                            @endforeach
                        </div>
                    @else
                        <div class="no-data">
                            {{ __('message.updating') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="grid-x video-container">
            <div class="small-12 large-10 large-offset-1 grid-content">
                <div class="text-title">
                    {{ __('message.featured_video') }}
                </div>
                <div class="body grid-x desktop">
                    @if(count($videos) > 0)
                        <div class="small-12 large-8 current-video-controller">
                            <img class="youtube-button" src="{{ asset('frontend/img/youtube-button.png') }}">
                            <img id="youtube-overlay" src="https://i.ytimg.com/vi/{{ $videos[0]->video_id  }}/hqdefault.jpg" />
                            <div class="description">
                                <span id="description-title">
                                    @if ($locale == 'en' && $videos[0]->title_en)
                                        {{ $videos[0]->title_en }}
                                    @else
                                        {{ $videos[0]->title }}
                                    @endif

                                </span>
                                <span id="description-time">{{ $videos[0]->time }}</span>
                            </div>
                            <iframe id="youtube-video" width="100%" height="100%"
                                    allowfullscreen
                                    src="https://www.youtube.com/embed/{{ $videos[0]->video_id }}" >
                            </iframe>
                        </div>
                        <div class="large-4 video-list">
                            <div class="title">
                                TOP VIDEO
                            </div>
                            <div class="body">
                                @foreach($videos as $video)
                                    <div class="grid-x video-item">
                                        <div class="medium-3 video-image">
                                            <div>
                                                <img src="https://i.ytimg.com/vi/{{ $video->video_id  }}/hqdefault.jpg" />
                                            </div>
                                        </div>
                                        <div class="medium-9 video-description">
                                            <div class="name">
                                                @if ($locale == 'en' && $video->title_en)
                                                    {{ $video->title_en }}
                                                @else
                                                    {{ $video->title }}
                                                @endif
                                            </div>
                                            <div class="time">
                                                <i class="fa fa-play" aria-hidden="true"></i> <span>{{ $video->time }}</span>
                                            </div>
                                        </div>
                                        <input type="hidden" class="vt" value="{{ $video->time }}">
                                        <input type="hidden" class="vn" value="  @if ($locale == 'en' && $video->title_en)
                                        {{ $video->title_en }}
                                        @else
                                        {{ $video->title }}
                                        @endif">
                                        <input type="hidden" class="vi" value="{{ $video->video_id }}">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @else
                        <div class="no-data">
                            {{ __('message.updating') }}
                        </div>
                    @endif

                </div>
                <div class="body grid-x mobile">
                    <div class="small-12 current-video-controller">
                        <div class="small-12 carousel-wrap large-10 large-offset-1 grid-content">
                            <div class="owl-carousel" id="video-carousel">
                                @foreach($videos as $video)
                                   <div class="mobile-video-item">
                                       <img class="mobile-youtube-button" src="{{ asset('frontend/img/youtube-button.png') }}">
                                       <img class="mobile-youtube-overlay" src="https://i.ytimg.com/vi/{{ $video->video_id  }}/hqdefault.jpg" />
                                       <div class="description">
                                           <span class="mobile-description-title">
                                                @if ($locale == 'en' && $video->title_en)
                                                   {{ $video->title_en }}
                                               @else
                                                   {{ $video->title }}
                                               @endif
                                           </span>
                                           <span class="mobile-description-time">{{ $video->time }}</span>
                                       </div>
                                       <iframe class="mobile-youtube-video" width="100%" height="100%"
                                               allowfullscreen
                                               src="https://www.youtube.com/embed/{{ $video->video_id }}" >
                                       </iframe>
                                       <input type="hidden" class="vi" value="{{ $video->video_id }}">
                                   </div>
                                @endforeach
                            </div>
                        </div>
                        {{--<img class="youtube-button" src="{{ asset('frontend/img/youtube-button.png') }}">--}}
                        {{--<img id="youtube-overlay" src="https://i.ytimg.com/vi/{{ $videos[0]->video_id  }}/maxresdefault.jpg" />--}}
                        {{--<div class="description">--}}
                            {{--<span id="description-title">{{ $videos[0]->title }}</span>--}}
                            {{--<span id="description-time">{{ $videos[0]->time }}</span>--}}
                        {{--</div>--}}
                        {{--<iframe id="youtube-video" width="100%" height="100%"--}}
                                {{--allowfullscreen--}}
                                {{--src="https://www.youtube.com/embed/{{ $videos[0]->video_id }}" >--}}
                        {{--</iframe>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-x register-form">
            <div class="small-12 large-10 large-offset-1 grid-content desktop">
                <div class="grid-x body">
                    <div class="small-12 medium-6 large-7 form-controller">
                        <div class="text-title small-12 global-title">
                            {{ __('message.home_sign_up') }}
                        </div>

                        <div class="small-12 medium-12 item">
                            <input id="fullname" placeholder="{{ __('message.fullname') }}" name="fullname" required />
                        </div>
                        <div class="small-12 item">
                            <input id="phone" placeholder="{{ __('message.phone') }}" name="phone" required />
                        </div>
                        <div class="small-12 item">
                            <input id="email" type="email" placeholder="Email" name="email" required />
                        </div>
                        <div class="small-12 item">
                            <select id="category" name="category" class="home-select">
                                @foreach($categories as $cat)
                                    <option value="{{ $cat->id }}">
                                        @if ($locale == 'en' && $cat->name_en)
                                            {{ $cat->name_en }}
                                        @else
                                            {{ $cat->name }}
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="small-12 item">
                            <button id="register" data-target="#event-modal">{{ __('message.send_information') }}</button>
                        </div>
                    </div>
                    <div class="small-12 medium-6 large-5 image-controller">
                        <img class="image desktop" src="{{ Voyager::image($avatarDesktop) }}">
                        <img class="image mobile" src="{{ Voyager::image($avatarMobile) }}">
                    </div>
                    {{--<div class="large-3">--}}
                    {{--<img class="image" src="{{ asset('frontend/img/girl.png') }}">--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="small-12 large-10 large-offset-1 grid-content mobile">
                <div class="grid-x body">
                    <form>
                        <div class="small-8 large-6 form-controller">
                            <div class="text-title small-12 global-title">
                               {{ __('message.home_sign_up') }}
                            </div>
                            <div class="small-12 medium-12 item">
                                <input id="fullname-mobile" placeholder="{{ __('message.fullname') }}" name="fullname" required />
                            </div>
                            <div class="small-12 item">
                                <input id="phone-mobile" placeholder="{{ __('message.phone') }}" name="phone" required />
                            </div>
                            <div class="small-12 item">
                                <input id="email-mobile" placeholder="Email" name="email" required />
                            </div>
                            <div class="small-12 item">
                                <select id="category-mobile" class="home-select" name="category">
                                    @foreach($categories as $cat)
                                        <option value="{{ $cat->id }}">
                                            @if ($locale == 'en' && $cat->name_en)
                                                {{ $cat->name_en }}
                                            @else
                                                {{ $cat->name }}
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <img class="image mobile" src="{{ Voyager::image($avatarMobile) }}">
                        <div class="small-12 item">
                            <button id="register-mobile" data-target="#event-modal">{{ __('message.send_information') }}</button>
                        </div>
                    </form>

                    {{--<div class="large-3">--}}
                    {{--<img class="image" src="{{ asset('frontend/img/girl.png') }}">--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        <?php
            $isHidePartner =  Voyager::setting('site.hide_partner', false)
        ?>
        @if(!$isHidePartner)
            <div class="grid-x partner-container">
            <div class="small-12 carousel-wrap large-10 large-offset-1 grid-content">
                <div class="owl-carousel" id="partner-carousel">
                    @foreach($partners as $partner)
                        <img src="{{ Voyager::image($partner->image) }}">
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        <div class="modal fade" id="event-modal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div id="modal-succeed">
                        <div class="modal-header">
                            <div class="header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body grid-x">
                                <div class="small-12">
                                    <img class="icon" src="{{ asset('frontend/img/succeed-icon.svg') }}" />
                                </div>
                                <div class="info">
                                    <div class="title">{{ Voyager::setting('site.modal_title') }}</div>
                                    <div class="description">
                                        {{ Voyager::setting('site.modal_message') }}
                                    </div>
                                    <button class="btn-sumit" data-dismiss="modal" id="btn-back">

                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript">
      $(document).ready(function() {
        var windowWidth = window.screen.width;
        var isAutoPlay = false;
        var timePlay = 5000;
        if (windowWidth < 800) {
          isAutoPlay = true;
        }
        function playVideo () {
          var videoURL = $('#youtube-video').prop('src');
          videoURL += "?autoplay=1";
          $('#youtube-video').prop('src',videoURL);
          $('#youtube-video').show();
        }
        $('#youtube-overlay').on('click', function(){
          $('.description').hide();
          $('.youtube-button').hide();
          $(this).hide();
          playVideo()
        });
        $('.youtube-button').on('touch click', function(){
          $('#youtube-overlay').hide();
          $('.description').hide();
          $(this).hide();
          playVideo();

        });
//        $('.service-item').hover(function() {
//          var parent = $(this).parent('.owl-item');
//          var isClicked = $(parent).hasClass('clicked');
//          $('#service-carousel').find('.owl-item').removeClass('clicked');
//          $(parent).addClass('clicked')
//        });

       /* $('.service-item').on('mouseover', function() {
          var parent = $(this).parent('.owl-item');
          var isClicked = $(parent).hasClass('clicked');
          $('#service-carousel').find('.owl-item').removeClass('clicked');
          $(parent).addClass('clicked')
        });*/
        $('.video-item').click(function() {
          var vt = $(this).find('.vt').val();
          var vn = $(this).find('.vn').val();
          var vi = $(this).find('.vi').val();
          $('#description-title').html(vn);
          $('.description').show();
          $('#description-time').html(vt);
          $('#youtube-overlay').attr('src', 'https://i.ytimg.com/vi/'+ vi +'/hqdefault.jpg');
          $('#youtube-video').hide();
          $('#youtube-video').attr('src', 'https://www.youtube.com/embed/'+ vi );
          $('.youtube-button').show();
          $('#youtube-overlay').show();
        });
        $('.mobile-video-item').click(function() {
          var vi = $(this).find('.vi').val();

          var videoURL = $(this).find('.mobile-youtube-video').prop('src');
          videoURL += "?autoplay=1";
          $(this).find('.mobile-youtube-video').prop('src',videoURL);
          $(this).find('.mobile-youtube-video').show();
          $(this).find('.description').hide();
          $(this).find('.mobile-youtube-overlay').hide();
          $(this).find('.mobile-youtube-button').hide();
        });
        $('#comment-carousel').owlCarousel({
//          loop:true,
          mouseDrag: false,
          touchDrag: true,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          autoplay: isAutoPlay,
          autoplayTimeout: timePlay,
          autoplayHoverPause:true,
          responsive: {
            0: {
              items: 1,
              nav: true
            }
          }
        });
        @if (count($banner) === 1)
            var isBannerLoop = false;
        @else
            var isBannerLoop = true;
        @endif

        $('#banner-carousel').owlCarousel({
          loop:isBannerLoop,
          mouseDrag: false,
          touchDrag: false,
          margin: 20,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          dots: true,
          autoplay: true,
          autoplayTimeout: timePlay,
//          autoplayHoverPause:true,
          responsive:{
            0:{
              items:1,
              nav:true
            },
          }
        });
        $('#service-carousel').owlCarousel({
          loop:true,
          mouseDrag: false,
          touchDrag: true,
          margin: 20,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          dots: true,
          autoplay: isAutoPlay,
          autoplayTimeout: timePlay,
          autoplayHoverPause:true,
          responsive:{
            0:{
              items:1,
              nav:true
            },
            600:{
              items:3,
              nav:true
            },
            1000:{
              items:4,
              nav:true,
              loop:true
            }
          }
        });
        $('#partner-carousel').owlCarousel({
          loop:true,
          mouseDrag: false,
          touchDrag: true,
          margin: 15,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          autoplay: isAutoPlay,
          autoplayTimeout: timePlay,
          autoplayHoverPause:true,
          responsive:{
            0:{
              items:3,
              nav:true
            },
            600:{
              items:3,
              nav:true
            },
            1000:{
              items:5,
              nav:true,
              loop:true
            }
          }
        });
        $('#video-carousel').owlCarousel({
          loop:true,
          mouseDrag: false,
          touchDrag: true,
          margin: 15,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          autoplay: isAutoPlay,
          autoplayTimeout: timePlay,
          autoplayHoverPause:true,
          responsive:{
            0:{
              items:1,
              nav:true
            }
          },
          onChanged: function() {
            $('.mobile-video-item').each(function() {
              var vi = $(this).find('.vi').val();
              $(this).find('.mobile-youtube-video').hide();
              $(this).find('.mobile-youtube-video').attr('src', 'https://www.youtube.com/embed/'+ vi );
              $(this).find('.description').show();
              $(this).find('.mobile-youtube-overlay').show();
              $(this).find('.mobile-youtube-button').show();
            })
          }
        });
        $('#change-page').click(function() {
          location.href = '/dinh-cu/' + $('#select-category').val();
        });
        function checkEmail(mail)
        {
          if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
          {
            return true;
          }
          return false;
        }
        $('#register').click(function(e) {
          e.preventDefault();
          var fullname = $('#fullname').val();
          var phone = $('#phone').val();
          var email = $('#email').val();
          var category = $('#category').val();
          if (!fullname) {
            alert('Vui lòng nhập họ và tên !');
            return false;
          }
          if (!phone) {
            alert('Vui lòng nhập số điện thoại !');
            return false;
          }
          if (!email) {
            alert('Vui lòng nhập email !');
            return false;
          }

          if (isNaN(phone) || phone.length !== 10) {
            alert('Số điện thoại không hợp lệ');
            return false;
          }
          if (!checkEmail(email)) {
            alert('Email không hợp lệ');
            return false;
          }
          $.ajaxSetup({

            headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
          });
          $.ajax({

            type:'POST',

            url:'{!! route('new-customer') !!}',

            data:{phone: phone, fullname: fullname, email: email, category: category},

            success:function(data){

              $('#event-modal').modal();
              $('#fullname').val('');
              $('#phone').val('');
              $('#email').val('');

            }

          });

        });
        $('#register-mobile').click(function(e) {
          e.preventDefault();
          var fullname = $('#fullname-mobile').val();
          var phone = $('#phone-mobile').val();
          var email = $('#email-mobile').val();
          var category = $('#category-mobile').val();
          if (!fullname) {
            alert('Vui lòng nhập họ và tên !');
            return false;
          }
          if (!phone) {
            alert('Vui lòng nhập số điện thoại !');
            return false;
          }
          if (!email) {
            alert('Vui lòng nhập email !');
            return false;
          }

          if (isNaN(phone) || phone.length !== 10) {
            alert('Số điện thoại không hợp lệ');
            return false;
          }
          if (!checkEmail(email)) {
            alert('Email không hợp lệ');
            return false;
          }
          $.ajaxSetup({

            headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
          });
          $.ajax({

            type:'POST',

            url:'{!! route('new-customer') !!}',

            data:{phone: phone, fullname: fullname, email: email, category: category},

            success:function(data){

              $('#event-modal').modal();
              $('#fullname-mobile').val('');
              $('#phone-mobile').val('');
              $('#email-mobile').val('');

            }

          });

        });
        $('#register-rc').click(function(e) {
          e.preventDefault();
          var email = $('#email-rc').val();
          if (!email) {
            alert('Vui lòng nhập email !');
            return false;
          }
          if (!checkEmail(email)) {
            alert('Email không hợp lệ');
            return false;
          }
          $.ajaxSetup({

            headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
          });
          $.ajax({

            type:'POST',

            url:'{!! route('new-customer') !!}',

            data:{phone: '', fullname: '', email: email, category: ''},

            success:function(data){

              $('#event-modal').modal();
              $('#email-rc').val('');
            }

          });

        });
        $('#register-mb').click(function(e) {
          e.preventDefault();
          var email = $('#email-mb').val();
          if (!email) {
            alert('Vui lòng nhập email !');
            return false;
          }
          if (!checkEmail(email)) {
            alert('Email không hợp lệ');
            return false;
          }
          $.ajaxSetup({

            headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
          });
          $.ajax({

            type:'POST',

            url:'{!! route('new-customer') !!}',

            data:{phone: '', fullname: '', email: email, category: ''},

            success:function(data){

              $('#event-modal').modal();
              $('#email-rc').val('');
            }

          });

        });
      });
    </script>
@stop
