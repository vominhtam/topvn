@extends('layouts.app')

@section('page_title')
    {{ __('message.Gallery') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop

@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="event-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' => __('message.Gallery')])
        <div class="large-10 large-offset-1 small-12 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' =>__('message.gallery_event'),
                    'url' => route('gallery')
                ],
                [
                    'title' => __('message.Gallery'),
                    'url' => ''
                ]
            ]])
            <div class="gallery-block grid-x small-12">
                <div class="large-10 large-offset-1 title grid-content">
                    GALLERY
                </div>
                <div class="grid-x gallery-list small-12">
                    <div class="small-12 grid-x group">
                    @foreach($galleries as $new)
                        <?php
                        $arrImage = json_decode($new->image);

                        ?>

                            @foreach($arrImage as $img)

                                <div class="small-12 medium-6 large-3 item item-gallery">
                                    <img src="{{ Voyager::image($img) }}">
{{--                                    <img src="{{ asset('frontend/img/gallery.png') }}" />--}}
                                </div>
                            @endforeach
                    @endforeach
                    </div>
                </div>
                <div style="display: none;" id="gallery">
                    @foreach($galleries as $g)
                        <?php $arrImage = json_decode($g->image); ?>
                        @foreach($arrImage as $item)
                            <a class="gal" href="{{ Voyager::image($item) }}">
                                <img src="{{ Voyager::image($item) }}" />
                            </a>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript')
    <script>
      $(document).ready(function() {
        $("#gallery").lightGallery();
        $('.item-gallery').click(function () {
          $('.gal').click();
        });
      });
    </script>
@stop
