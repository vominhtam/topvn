@extends('layouts.app')

@section('page_title')
    {{ __('message.Investment') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('url')
    {{ setting('site.description') }}
@stop
<?php
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="education-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' => $categoryName])
        <div class="large-10 large-offset-1 small-12 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' => __('message.Investment'),
                    'url' => ''
                ]
            ]])
            <div class="grid-x new-education">
                @foreach($news as $eb)
                    <div class="large-3 medium-6 small-12 item">
                        <img class="avatar" src="{{ Voyager::image($eb['image'])  }}" />
                        <div class="info">
                            <a href="{{ route('investment-detail', $eb->slug ? $eb->slug :$eb->id) }}">
                                <div class="title">
                                    @if ($locale == 'en' && $eb['title_en'])
                                        {{ $eb['title_en'] }}
                                    @else
                                        {{ $eb['title'] }}
                                    @endif

                                </div>
                            </a>

                            <div class="time-container grid-x">
                                <div class="small-4 medium-6">
                                    <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                    <span>{{ Carbon\Carbon::createFromTimestamp(strtotime($eb['created_at']))->diffForHumans() }}</span>
                                </div>
                                <div class="small-4 medium-6" style="text-align: right; color: black; font-weight: bold; margin-top: 10px">
                                    <span style="text-align: right; color: black; font-weight: bold"> {{ $eb['price'] }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $news->links() }}
        </div>
    </div>
@stop
