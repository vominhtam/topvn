@extends('layouts.app')

@section('page_title')
   {{ __('message.Education') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
@php


@endphp
<?php
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="new-detail-page grid-x">
        @include('layouts.banner_detail', ['banner' => $banner, 'title' => $locale == 'en' && $new->title_en ? $new->title_en : $new->title, 'data' => $new, 'category' => $new->type])
        <div class="grid-x large-10 large-offset-1 xlarge-8 xlarge-offset-2 small-12 small-offset-0">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' => __('message.Education'),
                    'url' => route('education')
                ],
                [
                    'title' => __('message.detail'),
                    'url' => ''
                ]
            ]])
            <div class="small-12">
                <img class="avatar" src="{{ Voyager::image($new->image) }}">
            </div>
            <div class="content-container">
                @if ($locale == 'en' && $new->content_en)
                    {!! $new->content_en !!}
                @else
                    {!! $new->content !!}
                @endif

            </div>
            <div class="small-12 viewed-new-container">
                <div class="title">{{ __('message.related_article') }}</div>
                <div class="carousel-wrap">
                    <div class="owl-carousel" id="viewed-new-carousel">
                        @foreach($news as $n)
                            <a href="{{ route('education-detail', $n->id) }}">
                                <div class="item">
                                    <img src="{{ Voyager::image($n->image) }}">
                                    <div class="info">
                                        <div class="category">
                                            {{ $n->type }}
                                        </div>
                                        <div class="title">
                                            @if ($locale == 'en' && $n->title_en)
                                                {{ $n->title_en }}
                                            @else
                                                {{ $n->title }}
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </a>



                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
      $(document).ready(function() {
        $('#viewed-new-carousel').owlCarousel({
          loop:true,
          mouseDrag: false,
          touchDrag: true,
          margin: 15,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          responsive:{
            0:{
              items:1,
              nav:true
            },
            600:{
              items:3,
              nav:true
            },
            1000:{
              items:4,
              nav:true,
              loop:true
            }
          }
        })
      })
    </script>

@stop
