@extends('layouts.app')

@section('page_title')
    {{ __('message.Education') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
@php
    $arrayHotEducation = $educationHot->toArray();
    $topEducation = [];
    if (count($educationHot) > 0) {
        $topEducation = $educationHot[0];
    }

    $rightEducation = array_slice($arrayHotEducation, 1, 4);
    $bottomEducation = array_slice($arrayHotEducation, 5, 4);

@endphp
<?php
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="education-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' => __('message.Education')])
        <div class="large-10 large-offset-1 small-12 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' => __('message.Education'),
                    'url' => ''
                ]
            ]])
            @if (count($arrayHotEducation))
                <div class="grid-x hot-new">
                @if($topEducation)
                    <div class="large-6 small-12 left-block">
                        <img src="{{ Voyager::image($topEducation->image) }}" />
                        <div class="info">

                            <div class="category">{{ __('message.new') }} </div>
                            <a href="{{ route('education-detail', $topEducation->id) }}">
                                <div class="title">
                                    @if ($locale == 'en' && $topEducation->title_en)
                                        {{ $topEducation->title_en }}
                                    @else
                                        {{ $topEducation->title }}
                                    @endif
                                </div>
                            </a>

                            <div class="author">
                                by <span>{{ $topEducation->author }}</span>
                            </div>
                            <div class="description">
                                @if ($locale == 'en' && $topEducation->description_en)
                                    {!! $topEducation->description_en !!}
                                @else
                                    {!! $topEducation->description !!}
                                @endif
                            </div>
                            <a href="{{ route('education-detail', $topEducation->id) }}"><span class="read-more">{{ __('message.read_more') }} ›</span></a>
                        </div>
                    </div>
                @endif
                <div class="large-6 small-12 right-block">
                    <div class="title-block">
                        {{ __('message.education_program') }}
                    </div>
                    <div class="event-list">
                        @foreach($rightEducation as $e)
                            <div class="small-12 grid-x event-item">
                                <div class="small-12 large-3 image">
                                    <img src=" {{ Voyager::image($e['image']) }}" />
                                </div>
                                <div class="small-12 large-9 info">
                                    <a href="{{ route('education-detail', $topEducation->id) }}">
                                        <div class="title">
                                            @if ($locale == 'en' && $e['title_en'])
                                                {{ $e['title_en'] }}
                                            @else
                                                {{ $e['title'] }}
                                            @endif
                                        </div>
                                    </a>

                                    <div class="time-container">
                                        <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                        <span>{{ Carbon\Carbon::createFromTimestamp(strtotime($e['created_at']))->diffForHumans() }}</span>
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                        <span> {{ $e['total_shared'] }} </span>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
                <div class="grid-x new-education">
                @foreach($bottomEducation as $eb)
                    <div class="large-3 medium-6 small-12 item">
                        <img class="avatar" src="{{ Voyager::image($eb['image'])  }}" />
                        <div class="info">
                            <a href="{{ route('education-detail', $topEducation->id) }}">
                                <div class="title">
                                    @if ($locale == 'en' && $e['title_en'])
                                        {{ $e['title_en'] }}
                                    @else
                                        {{ $e['title'] }}
                                    @endif
                                </div>
                            </a>

                            <div class="time-container grid-x">
                                <div class="small-4 medium-6">
                                    <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                    <span>{{ Carbon\Carbon::createFromTimestamp(strtotime($e['created_at']))->diffForHumans() }}</span>
                                </div>
                                <div class="small-4 medium-6">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                    <span> {{ $e['total_shared'] }} {{ __('message.share') }} </span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
                <div class="grid-x succeed-education">
                <div class="small-12 block-title">
                    {{ __('message.success_academy') }}
                </div>
                <div class="small-12 grid-x succeed-list">
                    @foreach($educationHot as $edu)
                        <div class="medium-4 small-12 item">
                            <img src="{{ Voyager::image($edu->image) }}" />
                            <div class="info">
                                <a href="{{ route('education-detail', $topEducation->id) }}">
                                    <div class="title">
                                        @if ($locale == 'en' && $edu->title_en)
                                            {{ $edu->title_en }}
                                        @else
                                            {{ $edu->title }}
                                        @endif

                                    </div>
                                </a>
                                <div class="author">
                                    by <span>{{ $edu->author }}</span>
                                </div>
                                <div class="time-container grid-x">
                                    <div class="small-12">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                        <span> {{ $edu['total_shared'] }} {{ __('message.share') }} </span>
                                    </div>
                                </div>
                                <div class="description">
                                    @if ($locale == 'en' && $edu->description_en)
                                        {!! $edu->description_en !!}
                                    @else
                                        {!! $edu->description !!}
                                    @endif
                                </div>
                                <a href="{{ route('education-detail', $topEducation->id) }}"><span class="read-more">{{ __('message.read_more') }} ›</span></a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            @else
                <div class="no-data">
                    {{ __('message.updating') }}
                </div>
            @endif
        </div>
    </div>
@stop
