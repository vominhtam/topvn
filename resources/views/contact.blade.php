@extends('layouts.app')

@section('page_title')
    {{ __('message.Contact') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="contact-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' => __('message.Contact')])
        <div class="large-10 large-offset-1 grid-content content">
            <div class="small-12" id="map"></div>
            <div class="contact-form">
               <div class="grid-x body">
                   <div class="small-12 medium-6 left">
                       <div class="title">
                           TopVnGroup
                       </div>
                       <?php
                            $address = Voyager::setting('site.address', '');
                           $arrayAddress = explode('|', $address);
                       ?>
                       @foreach($arrayAddress as $add)
                           <div class="item">
                               <img src="{{ asset('frontend/img/contact-icon1.svg') }}">
                               <span>{{ $add }}</span>
                           </div>
                       @endforeach
                       <?php
                            $email = Voyager::setting('site.email', '');
                            $arrayEmail = explode('|', $email);
                       ?>
                       @foreach($arrayEmail as $em)
                           <div class="item">
                               <img src="{{ asset('frontend/img/contact-icon2.svg') }}">
                               <span>{{ $em }}</span>
                           </div>
                       @endforeach
                       <?php
                            $phone = Voyager::setting('site.phone', '');
                            $arrayPhone = explode('|', $phone);
                       ?>
                       @foreach($arrayPhone as $ph)
                           <div class="item">
                               <img src="{{ asset('frontend/img/contact-icon3.svg') }}">
                               <span>{{ $ph }}</span>
                           </div>
                       @endforeach
                   </div>
                   <div class="small-12 medium-6 right">
                        <div class="item">
                            <input id="fullname" placeholder="{{ __('message.fullname') }}"/>
                        </div>
                       <div class="item">
                           <input id="email" placeholder="Email"/>
                       </div>
                       <div class="item">
                           <input id="phone" placeholder="{{ __('message.phone') }}"/>
                       </div>
                       <div class="item">
                           <input id="title" placeholder="{{ __('message.title') }}"/>
                       </div>
                       <div class="item">
                           <textarea id="content" placeholder="{{ __('message.content') }}"></textarea>
                       </div>
                       <div class="item">
                           <button id="btn-contact">{{ __('message.send_contact') }}  ›</button>
                       </div>
                       <img src="{{ asset('frontend/img/contact-social.svg') }}">
                   </div>
               </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="event-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div id="modal-succeed">
                    <div class="modal-header">
                        <div class="header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body grid-x">
                            <div class="small-12">
                                <img class="icon" src="{{ asset('frontend/img/succeed-icon.svg') }}" />
                            </div>
                            <div class="info">
                                <div class="description">
                                    <div class="title">CẢM ƠN BẠN ĐÃ LIÊN HỆ</div>
                                    <div class="description">
                                        Chúng tôi sẽ liện hệ với bạn trong thời gian sớm nhất
                                    </div>
                                </div>
                                {{--<button class="btn-sumit" data-dismiss="modal" id="btn-back">--}}
                                    {{--Trơ về trang chủ--}}
                                {{--</button>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('javascript')
    <script>
      var loca = "{{ setting('site.location') }}";
      var address = "{{ setting('site.address')  }}";
      var arrayAddess = address.split('|');

      var locations = loca.split('|');
      var arrayLocation = [];
      for (var i in locations) {
        var item = locations[i].split(',');
        arrayLocation.push({
          lat: item[0],
          lng: item[1]
        })
      }
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: parseFloat(arrayLocation[0].lat), lng: parseFloat(arrayLocation[0].lng)},
          zoom: 16
        });

        for (var i in arrayLocation) {
          var local = arrayLocation[i];
          var myLatLng = {lat: parseFloat(local.lat), lng: parseFloat(local.lng)};
          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!'
          });
          var content = '<div class="grid-x map-info">' +
            '<div class="small-4 medium-3" >' +
            '<img src="{{ Voyager::image(setting('site.logo'))  }}" />'+
            '</div>'+
            '<div class="small-8 medium-9">' +
            '<span>'+ arrayAddess[i] +'</span>' +
            '</div>'+
            '</div>';
          var infowindow = new google.maps.InfoWindow();
          if (i == 0) {
            infowindow.setContent(content);
            infowindow.open(map,marker);
          }
          google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
            return function() {
              infowindow.setContent(content);
              infowindow.open(map,marker);
            };
          })(marker,content,infowindow));
        }


      }
      $(document).ready(function() {
        $('#map').click(function() {

          var url = 'https://maps.google.fr/maps?saddr={{ setting('site.location') }}&daddr={{ setting('site.address') }}';
          var win = window.open(url, '_blank');
          win.focus();
        })
        $('#btn-contact').click(function(e) {
          e.preventDefault();
          var fullname = $('#fullname').val();
          var phone = $('#phone').val();
          var email = $('#email').val();
          var title = $('#title').val();
          var content = $('#content').val();
          if (!fullname) {
            alert('Vui lòng nhập họ và tên !');
            return false;
          }
          if (!phone) {
            alert('Vui lòng nhập số điện thoại !');
            return false;
          }
          if (!email) {
            alert('Vui lòng nhập email !');
            return false;
          }
          if (!title) {
            alert('Vui lòng nhập chủ đề !');
            return false;
          }
          if (!content) {
            alert('Vui lòng nhập content !');
            return false;
          }

          if (isNaN(phone) || phone.length !== 10) {
            alert('Số điện thoại không hợp lệ');
            return false;
          }
          if (!checkEmail(email)) {
            alert('Email không hợp lệ');
            return false;
          }
          $.ajaxSetup({

            headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
          });
          $.ajax({

            type:'POST',

            url:'{!! route('new-contact') !!}',

            data:{phone: phone, fullname: fullname, email: email, title: title, content: content},

            success:function(data){

              $('#event-modal').modal();
              $('#fullname').val('');
              $('#phone').val('');
              $('#email').val('');
              $('#title').val('');
              $('#content').val('');

            }

          });

        })
      });
      function checkEmail(mail)
      {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
        {
          return true;
        }
        return false;
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOeqn_QRWjq-d3jjbmgsgI2hPAymkfZuc&callback=initMap"
            async defer></script>
@stop
