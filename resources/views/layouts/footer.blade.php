<footer class="grid-x footer">
    <div class="small-12 large-10 large-offset-1 grid-content grid-x">
        <div class="large-2 small-12 ">
            <div class="item">
                <img class="logo" src="{{ asset('frontend/img/logo-footer.png') }}" />
            </div>
        </div>
        <div class="large-10 small-12 grid-x body">
            <div class="large-4 small-12 ">
                <div class="item description">
                    {{ setting('site.description', '')  }}
                </div>
            </div>
            <div class="large-4 small-12 ">
                <?php
                    $address = Voyager::setting('site.address', '');
                    $arrayAddress = explode('|', $address);
                ?>
                @foreach($arrayAddress as $add)
                        <div class="item address-container address">
                            <img src="{{ asset('frontend/img/location.svg')  }}" />
                            <div class="text">{{ $add }}</div>
                        </div>
                @endforeach

                <div class="item address-container">
                    <img src="{{ asset('frontend/img/zalo.svg')  }}" />
                    <div class="text">{{Voyager::setting('site.phone', '')}}</div>
                </div>
                <div class="item address-container">
                    <img src="{{ asset('frontend/img/mail.png')  }}" />
                    <div class="text">{{Voyager::setting('site.email', '')}}</div>
                </div>
            </div>
            <div class="large-4 small-12 ">
                <div class="item">
                    <div class="language-container">
                        <a href="{{ url('locale/vi') }}">
                            <img class="img" src="{{ asset('frontend/img/vietnam-flag.png')  }}" />
                        </a>
                        <a href="{{ url('locale/en') }}">
                            <img class="img" src="{{ asset('frontend/img/england-flag.png')  }}" />
                        </a>
                    </div>
                    <div class="social-container">
                        <a href="{{ Voyager::setting('site.facebook_page') }}" target="_blank" >
                            <img src="{{ asset('frontend/img/facebook-icon.svg') }}">
                        </a>
                        <a href="{{ Voyager::setting('site.google') }}">
                            <img src="{{ asset('frontend/img/google-icon.svg') }}">
                        </a>
                        <a href="{{ Voyager::setting('site.youtube') }}" target="_blank" >
                            <img src="{{ asset('frontend/img/youtube-icon.svg') }}">
                        </a>

                    </div>
                    {{--<div class="description">--}}
                        {{--Subscribe now and get 10% off and free delivery. Join today!--}}
                    {{--</div>--}}
                    <div class="input-container">
                        <input placeholder="Email của bạn" />
                        <a class="btn-send">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>

        <div class="small-12 copyright">
            © 2019 TOPVNGROUP
        </div>
    </div>
</footer>
