<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7"><![endif]-->
<!--[if IE 8]><html class="ie ie8"><![endif]-->
<!--[if IE 9]><html class="ie ie9"><![endif]-->
<html lang="{{ app()->getLocale() }}">
  <head>
    <!--
    Developed by: Vo Minh Tam
    Version 2.0
    -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <meta name="author" content="">
    <meta name="keywords" content="{{Voyager::setting('site.keywords')}}">
    <meta name="description" content="@yield('description')">
      <?php $facebook_image = Voyager::setting('site.facebook_image', ''); ?>
    @hasSection('facebook_title')
      <meta property="og:title" content="@yield('facebook_title')" />
    @else
      <meta property="og:title" content="{{ Voyager::setting('site.facebook_title') }}" />
    @endif
    @hasSection('facebook_image')
      <meta property="og:image" content="@yield('facebook_image')" />
    @else
      <meta property="og:image" content="{{ Voyager::image($facebook_image)}}" />
    @endif
    @hasSection('facebook_description')
      <meta property="og:description"
            content="@yield('facebook_description')" />
    @else
      <meta property="og:description"
            content="{!! Voyager::setting('site.facebook_description') !!}" />
    @endif

    <meta property="og:url" content="@yield('url')" />

    <title>@yield('page_title', setting('site.title') . " - " . setting('site.description'))</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500,bold,600,800,900&display=swap' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto Condensed:400,700,500,bold,600,800,900&display=swap' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('frontend/plugins/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/plugins/bootstrap-4.1.3/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/plugins/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/plugins/owl-carousel/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/plugins/owl-carousel/assets/owl.carousel.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/plugins/lightGallery-master/dist/css/lightgallery.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/plugins/bootstrap-nav/css/bootnavbar.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('frontend/plugins/select2/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/plugins/step/step.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/fonts/sanfrancisco-font.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v4.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

    @yield('head_css')
    <!--HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
    <!--[if IE 7]><body class="ie7 lt-ie8 lt-ie9 lt-ie10"><![endif]-->
    <!--[if IE 8]><body class="ie8 lt-ie9 lt-ie10"><![endif]-->
    <!--[if IE 9]><body class="ie9 lt-ie10"><![endif]-->
  <body class="@yield('class_body')">

    @yield('content')
    @include('layouts.footer')
    <div class="contact-con grid-x">
      <div  class="small-4 item" >
        <a href="tel:{{ Voyager::setting('site.hotline', '') }}">
          <img src="{{ asset('frontend/img/hot-line.svg') }}">
        </a>
      </div>

      <div class="small-4 item">
        <a href="{{ route('register') }}">
          <img src="{{ asset('frontend/img/clipboard.svg') }}">
        </a>
      </div>
      <div class="small-4 item" id="facebook-button">
        <img src="{{ asset('frontend/img/messenger.svg') }}">
      </div>

    </div>
    <!-- Your customer chat code -->
    <div class="fb-customerchat"
         greeting_dialog_display="hide"
         attribution=setup_tool
         page_id="112592986826858"
         theme_color="#ffc300"
         logged_in_greeting="Xin Chào"
         logged_out_greeting="Xin Chào">
    </div>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
    <script src="{{ asset('frontend/plugins/jquery-1.12.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="{{ asset('frontend/plugins/bootstrap-4.1.3/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('frontend/plugins/waypoint/imakewebthings-waypoints-34d9f6d/lib/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('frontend/plugins/jquery.countTo.js') }}"></script>
    <script src="{{ asset('frontend/plugins/jquery.matchHeight-min.js') }}"></script>
    <script src="{{ asset('frontend/plugins/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('frontend/plugins/lightGallery-master/dist/js/lightgallery-all.min.js') }}"></script>
    <script src="{{ asset('frontend/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('frontend/plugins/smooth-scroll.min.js') }}"></script>
    <script src="{{ asset('frontend/plugins/bootstrap-nav/js/bootnavbar.js') }}"></script>
    <script src="{{ asset('frontend/plugins/lory/jquery.lory.js') }}"></script>
    <script src="{{ asset('frontend/plugins/step/step.min.js') }}"></script>
    <script>
      $(function () {
        $('#main_navbar').bootnavbar();
      });
      $(document).ready(function(){
        $('#facebook-button').click(function() {
          var iframe = $('#fb-root .fb-customerchat').find('iframe');
          var className = iframe.attr('class');
          if (iframe.hasClass('fb_customer_chat_bounce_in_v2')) {
            FB.CustomerChat.hide()
          } else {
            FB.CustomerChat.show()
          }
//          if (isFirst) {
//
//            setTimeout(function(){
//              $('#facebook-inbox-frame').show(100)
//              $('.facebook-inbox-tab').hide(100)
//              $('.f-close').show(100)
//              isFirst = false;
//            }, 300);
//          } else {
//            $('#facebook-inbox-frame').show(100)
//            $('.facebook-inbox-tab').hide(100)
//            $('.f-close').show(100);
//          }


        });
        $('#left-nav').hide();
        $('#left-nav').click(function(e) {
          var container = $("#left-nav");
          console.log(!container.is(e.target));
          if (container.is(e.target)) {
            $('#left-nav').toggle();
          }
        })
        $('#sub-nav-toggle').click(function() {
          $('#left-nav').toggle();
        })
      })
    </script>
    <!-- Custom scripts-->
    @yield('javascript')
  </body>
</html>
