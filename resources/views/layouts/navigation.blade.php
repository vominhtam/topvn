@php
    $class = "navbar navbar-expand-lg navbar-light";
    if($isHome) {
        $class .= ' home-nav';
    }
@endphp
<nav class="{{ $class }}" id="main_navbar">
    @if($isHome)
        <a id="nav-logo-home" class="navbar-brand" href="{{ route('home') }}">
            <?php $logo_img = Voyager::setting('site.logo_home', ''); ?>
            <img src="{{ Voyager::image($logo_img ) }}">
        </a>
        <a id="nav-logo" class="navbar-brand normal" href="{{ route('home') }}">
            <?php $logo_img = Voyager::setting('site.logo', ''); ?>
            <img src="{{ Voyager::image($logo_img) }}">
        </a>
    @else
        <a class="navbar-brand normal" href="{{ route('home') }}">
            <?php $logo_img = Voyager::setting('site.logo', ''); ?>
            <img src="{{ Voyager::image($logo_img) }}">
        </a>
    @endif
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse grid-x" id="navbarSupportedContent">

        <div class="hotline-container small-12">
            <?php $phone_img = Voyager::setting('site.phone_icon', ''); ?>

                <img class="phone-icon" src="{{ Voyager::image($phone_img ) }}">
            <span class="phone">
                {{Voyager::setting('site.phone', '')}}
            </span>
            <div class="language-container">
                <a href="{{ url('locale/vi') }}">
                    <img class="img" src="{{ asset('frontend/img/vietnam-flag.png')  }}" />
                </a>
                <a href="{{ url('locale/en') }}">
                    <img class="img" src="{{ asset('frontend/img/england-flag.png')  }}" />
                </a>
            </div>

        </div>
        @if($isHome)
            <div class="small-12 medium-6 large-12">
                {!! menu('header', null, ['categories' => $categories]) !!}
            </div>

            {{--<div class="action-container small-12 large-2">--}}
                {{--<i class="fa fa-search" aria-hidden="true"></i>--}}
            {{--</div>--}}
        @else
            <div class=" large-12 small-12 medium-6">
                {!! menu('header', null, ['categories' => $categories]) !!}
            </div>
        @endif

    </div>

</nav>
