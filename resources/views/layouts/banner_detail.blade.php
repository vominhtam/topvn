<div class="banner-top detail">
    <span id="sub-nav-toggle" class="navbar-toggler-icon"> <img src="{{ asset('frontend/img/menu-icon.svg') }}" /></span>
    <div class="cover"></div>
    <div class="info grid-x">
        <div class="ext-center category">{{ $category }}</div>
        <div class="title">{{ $title }}</div>
        <div class="large-4 large-offset-4 medium-6 medium-offset-3 social-container">
            <div class="item medium-5">
                <img src="{{ asset('frontend/img/oclock.svg') }}">
                <span>{{ date("M d, h:m a",strtotime($data->created_at)) }}</span>
            </div>
            <div class="item medium-3">
                <img src="{{ asset('frontend/img/heart.svg') }}">
                <span>830</span>
            </div>
            <div class="item medium-3">
                <img src="{{ asset('frontend/img/comment.svg') }}">
                <span>43</span>
            </div>
            <div class="item medium-3">
                <img src="{{ asset('frontend/img/spy.svg') }}">
                <span>200</span>
            </div>

        </div>
    </div>

    <img class="bg" src="{{ Voyager::image($banner->image) }}">

</div>
