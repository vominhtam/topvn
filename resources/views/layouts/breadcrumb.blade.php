<div class="breadcrumb-container">
    @foreach($items as $index => $item)
        @if($index !== count($items) - 1)
            <a href="{{ $item['url'] }}" class="breadcrumb-item">
                {{ $item['title']  }}
            </a>
            <span class="flag">/</span>
        @else
            <span class="breadcrumb-item">{{ $item['title'] }}</span>
        @endif

    @endforeach

</div>
