<header>
    <div class="grid-x navigation-wrapper">
        <div class="small-12 large-10 large-offset-1 navigation-container grid-content">
            @include('layouts.navigation', ['isHome' => $isHome, 'categories' => $categories])
        </div>
    </div>
    <div id="left-nav" tabindex="0">
        <div class="body">
            <div class="header grid-x">
                <a  href="{{ route('home') }}">
                    <div class="logo">
                        <?php $logo_img = Voyager::setting('site.logo_home', ''); ?>
                        <img src="{{ Voyager::image($logo_img ) }}">
                    </div>
                </a>
            </div>
            <div class="menu-container">
                {!! menu('header', null, ['categories' => $categories]) !!}
            </div>
            <div class="social-container">
                <a href="{{ Voyager::setting('site.facebook_page') }}" target="_blank" >
                    <img src="{{ asset('frontend/img/facebook-icon.svg') }}">
                </a>
                <a>
                    <img src="{{ asset('frontend/img/google-icon.svg') }}">
                </a>
                <a href="{{ Voyager::setting('site.youtube') }}" target="_blank" >
                    <img src="{{ asset('frontend/img/youtube-icon.svg') }}">
                </a>
            </div>
            <div class="position-container">
                <div class="address-container">
                    <img src="{{ asset('frontend/img/location.svg')  }}" />
                    <div class="text">{{Voyager::setting('site.address', '')}}</div>
                </div>
                <div class="address-container">
                    <img src="{{ asset('frontend/img/zalo.svg')  }}" />
                    <div class="text">{{Voyager::setting('site.phone', '')}}</div>
                </div>
                <div class="address-container">
                    <img src="{{ asset('frontend/img/mail.png')  }}" />
                    <div class="text">{{Voyager::setting('site.email', '')}}</div>
                </div>
            </div>

        </div>
    </div>
</header>
