@extends('layouts.app')

@section('page_title')
   {{ __('message.article_detail') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    @if($new->meta)
        {!! $new->meta !!}
    @else
        {!! Voyager::setting('site.facebook_description') !!}
    @endif
@stop
@section('url')
    @if($new->slug)
        {{ route('new-detail', $new->slug) }}
    @else
        {{ route('new-detail', $new->id) }}
    @endif
@stop
@section('facebook_title')
    {!! $new->title !!}
@stop
@section('facebook_image')
    {!! Voyager::image($new->image) !!}
@stop
@section('facebook_description')
    {!! $new->description !!}
@stop
@php
@endphp
<?php
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="new-detail-page grid-x">
        @include('layouts.banner_detail', ['banner' => $banner, 'title' => $locale == 'en' && $new->title_en ? $new->title_en :  $new->title, 'data' => $new, 'category' => $new->category ? $new->category->name : ''])
        <div class="grid-x large-10 large-offset-1 xlarge-8 xlarge-offset-2 small-12 small-offset-0">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' => __('message.News'),
                    'url' => route('new')
                ],
                [
                    'title' => __('message.article_detail'),
                    'url' => ''
                ]
            ]])
            <div class="image-cover">
                <img src="{{ Voyager::image($new->image) }}">
            </div>
            <div class="content-container">
                @if ($locale == 'en' && $new->content_en)
                    {!! $new->content_en !!}
                @else
                    {!! $new->content !!}
                @endif
            </div>
            <div class="small-12 viewed-new-container">
                <div class="title">{{ __('message.related_article') }}</div>
                <div class="carousel-wrap">
                    <div class="owl-carousel" id="viewed-new-carousel">
                        @foreach($news as $n)
                            <div class="item">
                                <a href="{{ route('new-detail', $n->slug ? $n->slug : $n->id) }}">
                                    <img src="{{ Voyager::image($n->image) }}">
                                </a>
                                <div class="info">
                                    @if($n->category)
                                        <div class="category">
                                            @if ($locale == 'en' &&  $n->category->name_en)
                                                {{ $n->category->name_en }}
                                            @else
                                                {{  $n->category->name }}
                                            @endif
                                        </div>
                                    @endif
                                    <a href="{{ route('new-detail', $n->slug ? $n->slug : $n->id) }}">
                                        <div class="title">
                                            @if ($locale == 'en' &&  $n->title_en)
                                                {{ $n->title_en }}
                                            @else
                                                {{ $n->title }}
                                            @endif
                                        </div>
                                    </a>

                                </div>
                            </div>


                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
      $(document).ready(function() {
        $('#viewed-new-carousel').owlCarousel({
          loop:true,
          mouseDrag: false,
          touchDrag: true,
          margin: 15,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          responsive:{
            0:{
              items:1,
              nav:true
            },
            600:{
              items:3,
              nav:true
            },
            1000:{
              items:4,
              nav:true,
              loop:true
            }
          }
        })
      })
    </script>

@stop
