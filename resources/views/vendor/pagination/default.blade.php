<div class="pagination">
    <!-- Previous Page Link -->
    @if ($paginator->onFirstPage())
        <div class="disabled control left"><span><img src="{{ asset('frontend/img/prev.svg') }}"></span></div>
    @else
        <div class="control left"><a href="{{ $paginator->previousPageUrl() }}" rel="prev"><img src="{{ asset('frontend/img/prev.svg') }}"></a></div>
    @endif

<!-- Pagination Elements -->
    @foreach ($elements as $element)
    <!-- "Three Dots" Separator -->
        @if (is_string($element))
            <li class="disabled"><span>{{ $element }}</span></li>
        @endif

    <!-- Array Of Links -->
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @php
                    $class = "item";
                    if($page == $paginator->currentPage()) {
                        $class .= " active";
                    }
                    if($page === count($element)) {
                        $class .= " last";
                    }
                @endphp
                @if ($page == $paginator->currentPage())
                    <div class="{{ $class  }}"><span>{{ $page }}</span></div>
                @else
                    <div class="{{ $class }}"><a href="{{ $url }}">{{ $page }}</a></div>
                @endif
            @endforeach
        @endif
    @endforeach

<!-- Next Page Link -->
    @if ($paginator->hasMorePages())
        <div class="control right"><a href="{{ $paginator->nextPageUrl() }}" rel="next"><img src="{{ asset('frontend/img/next.svg') }}"></a></div>
    @else
        <div class="disabled control right"><span><img src="{{ asset('frontend/img/next.svg') }}"></span></div>
    @endif
</div>
