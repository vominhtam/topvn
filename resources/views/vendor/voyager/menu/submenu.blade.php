@php
    $dropdownMenuClass = "dropdown-menu";
    if ($level === 2) {
        $dropdownMenuClass .= ' level2';
    }
@endphp
<ul class="{{ $dropdownMenuClass }}" aria-labelledby="{{ $name  }}">
    @foreach ($items as $item)
        @php
            $className = null;
            $originalItem = $item;
            if (!$originalItem->children->isEmpty()) {
                $className = 'dropdown';
            }
            $name = str_replace("/", "", $item->link());
            if (!$item) {
                $name = "home";
            }
            if (strpos($item->link(), 'dinh-cu')
                || (strpos($item->link(), 'dau-tu'))
        ) {
                $temp = explode('/', $item->link());
                foreach ($options->categories as $cate) {
                        if ($cate->slug == $temp[count($temp) - 1]){
                            if ($options->locale == 'vi') {
                                 $item->title = $cate->name;
                            } else {
                                 $item->title = $cate->name_en;
                            }
                        }
                }
        }

        @endphp
        @if($originalItem->children->isEmpty())
            <li><a class="dropdown-item" href="{{ url($item->link()) }}"><i class="fa fa-chevron-right icon" aria-hidden="true"></i>{{ $item->title  }}</a></li>
        @else
            <li class="dropdown">

                <a class="dropdown-item dropdown-toggle" href="{{ $item->link()  }}" id="{{ $name }}" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-chevron-right icon" aria-hidden="true"></i>
                    {{ $item->title }}
                </a>
                @include('voyager::menu.submenu', ['items' => $originalItem->children, 'options' => $options, 'level' => 2])
            </li>
        @endif
    @endforeach
</ul>
