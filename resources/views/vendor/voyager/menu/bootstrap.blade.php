<div class="large-8 large-offset-2 small-10 small-offset-1 grid-x">
    @php

        if (Voyager::translatable($items)) {
            $items = $items->load('translations');
        }

    @endphp
    <div class="medium-12 large-9 grid-x">
        @foreach($items as $key => $item)
            @php
                $originalItem = $item;
            @endphp
            <div class="medium-3 item small-6">
                <div class="title">
                    {{ $item->title }}
                </div>
                @if(!$originalItem->children->isEmpty())
                    @foreach($originalItem->children as $keysub => $subitem)
                        <div class="subtitle">
                            @if($key === 3)
                                @if($keysub === 0)
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                @elseif($keysub === 1)
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                @elseif($keysub === 2)
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                @else
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                @endif
                            @endif
                            {{ $subitem->title }}
                        </div>
                    @endforeach
                @endif
            </div>
        @endforeach
    </div>
    <div class="large-3 small-12 ">
        <div class="item">
            <img src="{{ asset('frontend/img/logo-footer.png') }}" />
            <div class="description">
                Subscribe now and get 10% off and free delivery. Join today!
            </div>
            <div class="input-container">
                <input placeholder="Email của bạn" />
                <a class="btn-send">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="small-12 copyright">
        © 2019 Topvngroup
    </div>
</div>

