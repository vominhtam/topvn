

<ul class="navbar-nav">

@php

    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }

@endphp

@foreach ($items as $item)
    @php
        $originalItem = $item;
        if (Voyager::translatable($item)) {
            $item = $item->translate($options->locale);
        }
        $isActive = "nav-item";
        $name = str_replace("/", "", $item->link());
        if (!$item) {
            $name = "home";
        }
        $styles = null;
        if(!$originalItem->children->isEmpty()){
            $isActive .= ' dropdown';
        }
        if(url($item->link()) == url()->current()
                || ($item->link() === 'dinhcu' && strpos(url()->current(), 'dinh-cu'))
                || ($item->link() === 'dautu' && strpos(url()->current(), 'dau-tu'))
        ){
            $isActive .= ' active';
        }
    @endphp
        <li class="{{ $isActive }}">
            @if($originalItem->children->isEmpty())
                <a class="nav-link" href="{{ url($item->link()) }}">{{ __('message.'.$item->title) }} </a>
            @else
                <a class="nav-link dropdown-toggle" href="{{ url($item->link()) }}" id="{{ $name  }}" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                        {{ __('message.'.$item->title) }}
                </a>
                @include('voyager::menu.submenu', ['items' => $originalItem->children, 'options' => $options, 'level' => 1 ])

            @endif
        </li>
@endforeach

</ul>
