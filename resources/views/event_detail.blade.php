@extends('layouts.app')

@section('page_title')
   {{ __('message.event_detail') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
<?php
$locale = \Session::get('locale');
?>

@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="event-detail-page grid-x">
        @include('layouts.banner_detail', ['banner' => $banner, 'title' => __('message.event_detail'), 'data' => $event, 'category' => $event->type === 'EVENT' ? __('message.Event') : __('message.talkshow')])
        <div class="grid-x large-10 large-offset-1 small-12 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' => __('message.Event'),
                    'url' => route('event')
                ],
                [
                    'title' => __('message.event_detail'),
                    'url' => ''
                ]
            ]])
            <div class="content-container">
                @if ($locale == 'en' && $event->content_en)
                    {!! $event->content_en !!}
                @else
                    {!! $event->content !!}
                @endif
            </div>
            <div class="grid-x register-form">
                <div class="content small-12">
                    <div class="text-title small-12">
                       {{ __('message.home_sign_up') }}
                    </div>
                    <div class="grid-x">
                        <div class="small-12 form-controller grid-x">
                            <div class="small-12 medium-6 left item">
                                <input id="fullname" placeholder="{{ __('message.fullname') }}" name="fullname" required />
                            </div>
                            <div class="small-12 medium-6 right item">
                                <input id="phone" placeholder="{{ __('message.phone') }}" name="phone" required />
                            </div>
                            <div class="small-12 item">
                                <input id="email" type="email" placeholder="Email" name="email" required />
                            </div>
                            <div class="small-12 item">
                                <select id="category" name="category" class="home-select">
                                    @foreach($categories as $cat)
                                        <option value="{{ $cat->id }}">
                                            @if ($locale == 'en' && $cat->name_en)
                                                {{ $cat->name_en }}
                                            @else
                                                {{ $cat->name }}
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="small-12 item">
                                <button id="register">{{ __('message.send_information') }}</button>
                            </div>
                        </div>
                    </div>
                    {{--<img class="image" src="{{ asset('frontend/img/girl.png') }}">--}}
                </div>
            </div>
            <div class= "small-12 gallery-block">
                <div class="block-title">
                   {{ __('message.gallery_of_event') }}
                </div>
                @if(count($galleries) > 0)
                    <div class="main">
                        <?php $arrayImg = json_decode($galleries[0]->image); ?>
                        <img id="galllery-main" src="{{Voyager::image($arrayImg[0])}}">
                    </div>
                    <div class="grid-x list">
                        <div class="owl-carousel" id="gallery-carousel">
                            @foreach($galleries as $index => $ga)
                                <?php $arrImage = json_decode($ga->image); ?>
                                @foreach($arrImage as $item)
                                        <img class="gallery-item" id="{{ $index  }}" src="{{ Voyager::image($item) }}">
                                @endforeach

                            @endforeach

                        </div>

                        <div class="pre">
                            <i class="fa fa-chevron-left icon" aria-hidden="true"></i>
                        </div>
                        <div class="next">
                            <i class="fa fa-chevron-right icon" aria-hidden="true"></i>
                        </div>
                    </div>
                @else
                    <div class="no-data">
                        {{ __('message.updating') }}
                    </div>
                @endif

            </div>
        </div>
        <div class="modal fade" id="event-modal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div id="modal-succeed">
                        <div class="modal-header">
                            <div class="header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body grid-x">
                                <div class="small-12">
                                    <img class="icon" src="{{ asset('frontend/img/succeed-icon.svg') }}" />
                                </div>
                                <div class="info">
                                    <div class="title">{{ Voyager::setting('site.modal_title') }}</div>
                                    <div class="description">
                                        {{ Voyager::setting('site.modal_message') }}
                                    </div>
                                    <button class="btn-sumit" data-dismiss="modal" id="btn-back">
                                        {{ __('message.back_to_home') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
      function checkEmail(mail)
      {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
        {
          return true;
        }
        return false;
      }
      $(document).ready(function(){
        var owl = $('#gallery-carousel').owlCarousel({
          loop:true,
          mouseDrag: false,
          touchDrag: true,
          margin: 15,
          navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
          responsiveClass:true,
          responsive:{
            0:{
              items:1,
              nav:true
            },
            600:{
              items:3,
              nav:true
            },
            1000:{
              items:5,
              nav:true,
              loop:true
            }
          }
        });
        $('.gallery-item').on('click', function(){
          $('#galllery-main').attr('src', this.src)
        });
        $('.pre').click(function () {
          owl.trigger('prev.owl.carousel');
        })
        $('.next').click(function () {
          owl.trigger('next.owl.carousel');
        })
        $('#register').click(function(e) {
          e.preventDefault();
          var fullname = $('#fullname').val();
          var phone = $('#phone').val();
          var email = $('#email').val();
          var category = $('#category').val();
          if (!fullname) {
            alert('Vui lòng nhập họ và tên !');
            return false;
          }
          if (!phone) {
            alert('Vui lòng nhập số điện thoại !');
            return false;
          }
          if (!email) {
            alert('Vui lòng nhập email !');
            return false;
          }

          if (isNaN(phone) || phone.length !== 10) {
            alert('Số điện thoại không hợp lệ');
            return false;
          }
          if (!checkEmail(email)) {
            alert('Email không hợp lệ');
            return false;
          }
          $.ajaxSetup({

            headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
          });
          $.ajax({

            type:'POST',

            url:'{!! route('new-customer') !!}',

            data:{phone: phone, fullname: fullname, email: email, category: category},

            success:function(data){

              $('#event-modal').modal();
              $('#fullname').val('');
              $('#phone').val('');
              $('#email').val('');

            }

          });

        });

      })
    </script>

@stop
