<thead>
<tr>
    <th></th>
    @foreach($arrayTitle as $title)
        <th>{{ $title }}</th>
    @endforeach
</tr>
</thead>
<tbody>
@foreach($arrayCompare as $compare)
    <tr>
        <td><img class="icon" src="{{ Voyager::image($compare['icon']) }}"/></td>
        <td>{{ $compare['value1'] }}</td>
        <td>{{ $compare['value2'] }}</td>
    </tr>
@endforeach
</tbody>
