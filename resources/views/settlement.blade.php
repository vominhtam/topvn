@extends('layouts.app')

@section('page_title')
  {{ __('message.settlement') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
@php


@endphp
<?php
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $category_all])
    <?php
        $name = $settlement->category ? $settlement->category->name : '';
        if ($locale == 'en' && $settlement->category && $settlement->category->name_en) {
            $name = $settlement->category ? $settlement->category->name_en : '';
        }
    ?>
    <div class="settlement-page grid-x">
        @include('layouts.banner_category', ['banner' => $settlement, 'title' => $name])
        <div class="grid-x large-10 large-offset-1 small-12 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => 'Trang chủ',
                    'url' => route('home')
                ],
                [
                    'title' =>  __('message.settlement').' '.$name,
                    'url' => ''
                ]
            ]])
            <div class="description">
                @if ($locale == 'en' && $settlement->description_en)
                    {!! $settlement->description_en !!}
                @else
                    {!! $settlement->description !!}
                @endif
            </div>
        </div>
        <div class="small-12 grid-x info-container">
            <div class="grid-x large-10 large-offset-1 small-12 grid-content">
                {{--<div class="compare-container" data-toggle="modal" data-target="#compare-modal">--}}
                    {{--<button class="btn btn-info btn-compare">Compare</button>--}}
                {{--</div>--}}
                <div class="medium-10 small-12 grid-x">
                    @foreach($settlement->icons as $icon)
                        <div class="item content-container">
                            <div class="icon-container">
                                <img class="icon" src="{{ Voyager::image($icon->icon->image) }}" />
                            </div>

                            <div class="text">{{ $icon->value }}</div>
                        </div>
                    @endforeach
                </div>
                <div class="medium-2 small-12 item button-container">
                    <button class="btn btn-info btn-compare" data-toggle="modal" data-target="#compare-modal">
                        <img src="{{ asset('/frontend/img/evolution.png') }}" />
                        So sánh
                    </button>
                </div>

            </div>
        </div>
        <div class="grid-x large-10 large-offset-1 small-12 grid-content">
            <div class="content">
                @if ($locale == 'en' && $settlement->content_en)
                    {!! $settlement->content_en !!}
                @else
                    {!! $settlement->content !!}
                @endif
            </div>
            <div id="stepper2" class="bs-stepper">
                <div class="bs-stepper-header">
                    @foreach($steps as $index => $value)
                        <div class="step" data-target="{{ "#test-nl-".$index }}">
                            <button type="button" class="btn step-trigger">
                                <span class="bs-stepper-circle">{{ $index + 1 }}</span>
                            </button>
                            <div style="text-align: center">{{ __('message.step') }} {{ $index + 1 }}</div>
                        </div>
                        @if($index !== count($steps) - 1)
                            <div class="line"></div>
                        @endif
                    @endforeach
                </div>
                <div class="bs-stepper-content">
                    @foreach($steps as $index => $value)
                        <div id="{{ "test-nl-".$index  }}" class="content">
                           <div class="text title">
                               @if ($locale == 'en' && $value->title_en)
                                   {{ $value->title_en }}
                               @else
                                   {{ $value->title }}
                               @endif

                           </div>
                           <div class="text description">
                               @if ($locale == 'en' && $value->description_en)
                                   {!! $value->description_en !!}
                               @else
                                   {!! $value->description !!}
                               @endif
                           </div>
                           @if ($index < count($steps) - 1)
                                <div class="btn-next">
                                    <button class="btn btn-info btn-compare">
                                        {{ __('message.next') }}
                                    </button>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            {{--<div class="small-12 news-container">--}}
                {{--<div class="title">Tin tức</div>--}}
                {{--<div class="news-list grid-x">--}}
                    {{--@foreach($events as $event)--}}
                        {{--@if($event->banner)--}}
                            {{--<div class="large-3 medium-6 small-12 grid-x item">--}}
                                {{--<img class="avatar" src="{{ Voyager::image($event->banner)  }}" />--}}
                                {{--<div class="info">--}}
                                    {{--<a href="{{ route('event-detail', $event->id) }}">--}}
                                        {{--<div class="title">{{ $event->title }}</div>--}}
                                    {{--</a>--}}
                                    {{--<div class="time-container grid-x">--}}
                                        {{--<div class="small-12">--}}
                                            {{--<img src="{{ asset('frontend/img/calendar2.svg') }}" />--}}
                                            {{--<span>{{ Carbon\Carbon::createFromTimestamp(strtotime($event->created_at))->diffForHumans() }}</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endif--}}



                    {{--@endforeach--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="small-12 viewed-new-container grid-x news-container">
                <div class="title">{{ __('message.settlement')  }}</div>
                <div class="carousel-wrap small-12">
                    <div class="owl-carousel" id="viewed-new-carousel">
                        @foreach($news as $n)
                            <div class="item">
                                <a href="{{ route('new-detail', $n->slug ? $n->slug : $n->id) }}">
                                    <img src="{{ Voyager::image($n->image) }}">
                                </a>

                                <div class="info">
                                    <div class="category">
                                        @if($n->category)
                                            <div class="category">
                                                @if ($locale == 'en' &&  $n->category->name_en)
                                                    {{ $n->category->name_en }}
                                                @else
                                                    {{  $n->category->name }}
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                    <a href="{{ route('new-detail', $n->slug ? $n->slug : $n->id) }}">
                                        <div class="title">
                                            @if ($locale == 'en' &&  $n->title_en)
                                                {{ $n->title_en }}
                                            @else
                                                {{  $n->title }}
                                            @endif
                                        </div>
                                    </a>


                                </div>
                            </div>


                        @endforeach
                    </div>
                </div>
            </div>
            <div class="small-12 news-container">
                <div class="title">{{ __('message.Investment') }}</div>
                <div class="news-list grid-x">
                    @foreach($events as $new)
                        @if($new->image)
                            <div class="large-3 medium-6 small-12 grid-x item">
                                <a href="{{ route('new-detail', $new->slug ? $new->slug : $new->id) }}">
                                    <img class="avatar" src="{{ Voyager::image($new->image)  }}" />
                                </a>
                                <div class="info">
                                    <a href="{{ route('new-detail', $new->slug ? $new->slug : $new->id) }}">
                                        <div class="title">
                                            @if ($locale == 'en' &&  $new->title_en)
                                                {{ $new->title_en }}
                                            @else
                                                {{  $new->title }}
                                            @endif
                                        </div>
                                    </a>
                                    <div class="time-container grid-x">
                                        <div class="small-12">
                                            <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                            <span>{{ Carbon\Carbon::createFromTimestamp(strtotime($new->created_at))->diffForHumans() }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif



                    @endforeach
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="compare-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                </div>
                <div class="modal-body grid-x">
                    <select class="select-category" id="select-category">
                        @foreach($categories as $cat)
                            <option value="{{ $cat->id }}">
                                @if ($locale == 'en' && $cat->name_en)
                                    {{ $cat->name_en }}
                                @else
                                    {{ $cat->name }}
                                @endif
                            </option>
                        @endforeach
                    </select>
                    <table class="table table-hover" id="table-content">
                        <thead>
                        <tr>
                            <th></th>
                            @foreach($arrayTitle as $title)
                                <th>{{ $title }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($arrayCompare as $compare)
                                <tr>
                                    <td><img class="icon" src="{{ Voyager::image($compare['icon']) }}"/></td>
                                    <td>{{ $compare['value1'] }}</td>
                                    <td>{{ $compare['value2'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
      var stepper2 = new Stepper(document.querySelector('#stepper2'), {
        linear: false,
        animation: true
      });
      $('#viewed-new-carousel').owlCarousel({
        loop:true,
        mouseDrag: false,
        touchDrag: true,
        margin: 15,
        navText: ['<i class="fa fa-chevron-left icon" aria-hidden="true"></i>', '<i class="fa fa-chevron-right icon" aria-hidden="true"></i>'],
        responsiveClass:true,
        responsive:{
          0:{
            items: 1,
            nav:true
          },
          1000:{
            items:4,
            nav:true,
            loop:true
          }
        }
      });
      $('.btn-next').click(function() {
        stepper2.next()
      });
        $(document).ready(function() {
          $('#select-category').on('change', function() {
            var value = $(this).val();
            $.ajaxSetup({

              headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

              }
            });
            $.ajax({

              type:'POST',

              url:'{!! route('get-settlement') !!}',

              data:{slug: '{{ $settlement->category->slug }}', settlementId: '{{ $settlement->id }}', categoryId: value },

              success:function(data){
                $('#table-content').html(data)

              }

            });
          })
        });
    </script>
@stop
