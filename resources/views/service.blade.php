@extends('layouts.app')

@section('page_title')
    {{ __('message.service') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
@php
    $arrayGallery = $gallery->toArray();
    $arrayImage = [];
    $arrayGallery = $galleries->toArray();
    foreach ($arrayGallery as $item) {
        $arrImg = json_decode($item['image']);
        foreach ($arrImg as $img) {
            $arrayImage[] = $img;
        }
    }
    $arr = array_chunk($arrayImage, 4);
@endphp
<?php
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="service-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' =>  __('message.service')])
        <div class="large-10 large-offset-1 small-12 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' =>  __('message.service'),
                    'url' => ''
                ]
            ]])
            <div class="content-container">
                @if ($locale == 'en' &&  $new->content_en)
                    {!! $new->content_en !!}
                @else
                    {!! $new->content !!}
                @endif


            </div>
            <div class="gallery-block grid-x">
                @foreach($arr as $index => $i)
                    <div class="small-12 medium-6 large-3 item item-gallery">
                        @if($index === count($arr) - 1)
                            <div class="cover">
                                + {{count($gallery)}} photos
                            </div>
                        @endif
                        <img src="{{ Voyager::image($i['image']) }}">
                    </div>
                @endforeach
            </div>
            <div style="display: none;" id="gallery">
                @foreach($gallery as $g)
                    <?php $arrServiceImage = json_decode($g->image); ?>
                    @foreach($arrServiceImage as $item)
                        <a class="gal" href="{{ Voyager::image($item) }}">
                            <img src="{{ Voyager::image($item) }}" />
                        </a>
                    @endforeach
                @endforeach
            </div>
        </div>
    </div>
@stop
@section('javascript')
    <script>

      $(document).ready(function(){
        $("#gallery").lightGallery();
        $('.item-gallery').click(function() {
          $('.gal').click();
        });
      })
    </script>
@stop
