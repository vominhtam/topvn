<a class="btn btn-info" id="export_btn"><i class="voyager-file-text"></i> <span>Export CSV</span></a>

{{-- export modal --}}
<div class="modal modal-info fade" tabindex="-1" id="export_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-file-text"></i> Are you sure you want to export <span id="export_count"></span> customer?
                </h4>
            </div>
            <div class="modal-body" id="export_modal_body">
            </div>
            <div class="modal-footer">
                <form action="{{ route('export_csv') }}" id="export_form" method="POST">
                    {{ method_field("POST") }}
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="export_input" value="">
                    <input type="submit" class="btn btn-info pull-right export-confirm"
                           value="Yes" >
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                   Cancel
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

