@extends('layouts.app')

@section('page_title')
    {{ __('message.recruitment') }}| {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
<?php
$locale = \Session::get('locale');
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="recruitment-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' =>  __('message.recruitment')])
        <div class="large-10 large-offset-1 small-12 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' =>  __('message.recruitment'),
                    'url' => ''
                ]
            ]])
            <div class="event-list">
                @foreach($news as $new)
                    <div class="small-12 grid-x event-item">
                        <div class="small-12 medium-3 image">
                           <img src=" {{ Voyager::image($new->image) }}" />
                        </div>
                        <div class="small-12 medium-9 info">
                            <div class="category">
                                @if($new->category)
                                    <div class="category">
                                        @if ($locale == 'en' &&  $new->category->name_en)
                                            {{ $new->category->name_en }}
                                        @else
                                            {{  $new->category->name }}
                                        @endif
                                    </div>
                                @endif
                            </div>
                            <div class="title">
                                @if ($locale == 'en' &&  $new->title_en)
                                    {{ $new->title_en }}
                                @else
                                    {{  $new->title }}
                                @endif
                            </div>
                            <div class="time-container">
                                <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                <span>{{ date("d/m/Y h:m a",strtotime($news[0]->created_at)) }}</span>
                            </div>
                            <div class="description-container">
                                <div class="description">
                                    @if ($locale == 'en' &&  $new->description_en)
                                        {{ $new->description_en }}
                                    @else
                                        {{  $new->description }}
                                    @endif
                                </div>
                                <a href="{{ route('new-detail', $new->slug ? $new->slug : $new->id) }}"><span class="read-more">{{ __('message.read_more') }}</span></a>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
            {{ $news->links() }}
        </div>
    </div>
@stop
