@extends('layouts.app')

@section('page_title')
   {{ __('message.Event') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
@php
    $latestEvent = count($events) > 0 ? $events[0] : null;
    $arrayEvents = $events->toArray();
    $arr = array_slice($arrayEvents, 1);
    $arrayImage = [];
    $arrayGallery = $galleries->toArray();
    foreach ($arrayGallery as $item) {
        $arrImg = json_decode($item['image']);
        foreach ($arrImg as $img) {
            $arrayImage[] = $img;
        }
    }
    $arrGa = array_chunk($arrayImage, 4);
    $locale = \Session::get('locale');
@endphp

@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="event-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' => __('message.Event')])
        <div class="grid-x large-10 large-offset-1 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' => __('message.Event'),
                    'url' => ''
                ]
            ]])
            @if ($latestEvent)
                <div class="small-12 latest-block">
                    <img class="banner" src="{{ Voyager::image($latestEvent->banner) }}">
                    <div class="info">
                        <div class="category">{{ __('message.latest_event') }} </div>
                        <a href="{{ route('event-detail', $latestEvent->id)  }}">
                            <div class="title">
                                @if ($locale == 'en' && $latestEvent->title_en)
                                    {{ $latestEvent->title_en }}
                                @else
                                    {{ $latestEvent->title }}
                                @endif
                            </div>
                        </a>

                        <div class="time-container">
                            <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                            <span>{{ date("d/m/Y h:m a",strtotime($latestEvent->created_at)) }}</span>
                        </div>
                        <div class="description">
                            @if ($locale == 'en' && $latestEvent->description_en)
                                {{ $latestEvent->description_en }}
                            @else
                                {{ $latestEvent->description }}
                            @endif
                        </div>
                    </div>
                </div>
            @else
                <div class="small-12 latest-block">
                </div>
            @endif
            <div class="large-4 medium-6 small-4 btn-register" data-toggle="modal" data-target="#event-modal" >
                {{ __('message.register_now') }}
            </div>
            <div class="event-list">
                <div class="title-block">
                    {{ __('message.other_outstanding_events') }}
                </div>
                @foreach($arr as $new)
                    <div class="small-12 grid-x event-item">
                        <div class="small-12 medium-3 image">
                           <img src=" {{ Voyager::image($new['banner']) }}" />
                        </div>
                        <div class="small-12 medium-9 info">
                            <div class="category">
                                {{ $new['type'] === 'EVENT' ? __('message.Event') : __('message.talkshow')  }}
                            </div>
                            <a href="{{ route('event-detail', $new['id'])  }}">
                                <div class="title">
                                    @if ($locale == 'en' && $new['title_en'])
                                        {{ $new['title_en'] }}
                                    @else
                                        {{ $new['title'] }}
                                    @endif
                                </div>
                            </a>
                            <div class="time-container">
                                <img src="{{ asset('frontend/img/calendar2.svg') }}" />
                                <span>{{ date("d/m/Y h:m a",strtotime($new['created_at'])) }}</span>
                            </div>
                            <div class="description-container">
                                <div class="description">
                                    @if ($locale == 'en' && $new['description_en'])
                                        {{ $new['description_en'] }}
                                    @else
                                        {{ $new['description'] }}
                                    @endif
                                </div>
                                <a  href="{{ route('event-detail', $new['id'])  }}" ><span class="read-more">{{ __('message.read_more') }}</span></a>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
{{--
        <div class="gallery-block grid-x small-12">
            <div class="large-10 large-offset-1 title grid-content">
                GALLERY
            </div>
            <div class="grid-x gallery-list small-12">
                @foreach($arrGa as $item)
                    <div class="small-12 grid-x group">
                        @foreach($item as $i)
                            <div class="small-12 medium-6 large-3 item item-gallery">
                                <img src="{{ Voyager::image($i) }}">
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>--}}
      {{--  <div style="display: none;" id="gallery">
            @foreach($galleries as $g)
                <?php $arrImage = json_decode($g->image); ?>
                @foreach($arrImage as $item)
                        <a class="gal" href="{{ Voyager::image($item) }}">
                            <img src="{{ Voyager::image($item) }}" />
                        </a>
                @endforeach
            @endforeach
        </div>--}}
        <div class="modal fade" id="event-modal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div id="modal-succeed">
                        <div class="modal-header">
                            <div class="header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body grid-x">
                            <div class="small-12">
                                <img class="icon" src="{{ asset('frontend/img/succeed-icon.svg') }}" />
                            </div>
                            <div class="info">
                                <div class="title">{{ __('message.thank_for_register') }}</div>
                                <div class="description">
                                    {{ __('message.check_email') }}
                                </div>
                                <button class="btn-sumit" data-dismiss="modal" id="btn-back">
                                   {{ __('message.back_to_home') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                    <div id="form-submit">
                        <div class="modal-header">
                            <div class="header">
                               {{ __('message.form_event') }}
                            </div>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body grid-x">
                            <div class="medium-6 small-12 item left">
                                <input placeholder="{{ __('message.fullname') }}" id="fullname" />
                            </div>
                            <div class="medium-6 small-12 item right">
                                <input placeholder="{{ __('message.phone') }}" id="phone" />
                            </div>
                            <div class="small-12 item">
                                <input placeholder="Email" id="email" />
                            </div>
                            <div class="small-12 item">
                                <select id="category" name="category" class="home-select">
                                    @foreach($categories as $cat)
                                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="small-12 item">
                                <button class="btn-sumit" id="btn-submit">
                                    {{ __('message.send') }}
                                </button>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
      $(document).ready(function(){
        $("#gallery").lightGallery();
        $('.item-gallery').click(function() {
          $('.gal').click();
        });
        $('#modal-succeed').hide();
        function checkEmail(mail)
        {
          if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
          {
            return true;
          }
          return false;
        }
        $('#btn-submit').click(function(e) {
          e.preventDefault();
          var fullname = $('#fullname').val();
          var phone = $('#phone').val();
          var email = $('#email').val();
          var category = $('#category').val();
          if (!fullname) {
            alert('Vui lòng nhập họ và tên !');
            return false;
          }
          if (!phone) {
            alert('Vui lòng nhập số điện thoại !');
            return false;
          }
          if (!email) {
            alert('Vui lòng nhập email !');
            return false;
          }

          if (isNaN(phone) || phone.length !== 10) {
            alert('Số điện thoại không hợp lệ');
            return false;
          }
          if (!checkEmail(email)) {
            alert('Email không hợp lệ');
            return false;
          }
          $.ajaxSetup({

            headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
          });
          $.ajax({

            type:'POST',

            url:'{!! route('new-customer') !!}',

            data:{phone: phone, fullname: fullname, email: email, category: category},

            success:function(data){

              $('#form-submit').hide();
              $('#modal-succeed').show();
              $('#fullname').val('');
              $('#phone').val('');
              $('#email').val('');

            }

          });

        });
        $('.close').click(function() {
          $('#form-submit').show();
          $('#modal-succeed').hide()
        });
        $('#btn-back').click(function() {
          $('#form-submit').show();
          $('#modal-succeed').hide();
        });
      })
    </script>

@stop
