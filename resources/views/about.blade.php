@extends('layouts.app')

@section('page_title')
    {{ __('message.About') }} | {{setting('site.title') . " - " . setting('site.description')}}
@stop
@section('description')
    {{ setting('site.description') }}
@stop
<?php
$locale = \Session::get('locale');
$str = '';
if ($locale == 'en') {
    $str = setting('site.company_info_en');
} else{
    $str = setting('site.company_info');
}
$arrInfo = explode('|', $str);
$strMission = '';
if ($locale == 'en') {
    $strMission = setting('site.company_mission_en');
} else{
    $strMission = setting('site.company_mission');
}
$arrMission = explode('|', $strMission);
$arrMissionData = [];
foreach ($arrMission as $key => $value) {
    $i = $key + 1;
    if ($locale == 'en') {
        $temp = 'site.misson_'.(string)$i.'_en';
    } else{
        $temp = 'site.misson_'.(string)$i;
    }
    $miss = setting($temp);
    $arrMissionData[] = [
      'title'=> $value,
      'text' => $miss,
    ];
}
?>
@section('content')
    @include('layouts.header', ['isHome' => false, 'categories' => $categories])
    <div class="about-page grid-x">
        @include('layouts.banner', ['banner' => $banner, 'title' => __('message.About')])
        <div class="small-12 large-10 large-offset-1 grid-content">
            @include('layouts.breadcrumb', ['items' => [
                [
                    'title' => __('message.home'),
                    'url' => route('home')
                ],
                [
                    'title' => __('message.About'),
                    'url' => ''
                ]
            ]])
            <div class="body grid-x">
                <div class="small-12 title">
                    @if ($locale == 'en')
                        {{ setting('site.title_introduce_page_en')  }}
                    @else
                        {{ setting('site.title_introduce_page')  }}
                    @endif

                </div>
                <div class="large-6 small-12  image">
                    <?php $logo_img = Voyager::setting('site.image_introduce_page', ''); ?>
                    <img src="{{ Voyager::image($logo_img ) }}" />
                </div>
                <div class="large-6 small-12 info">
                    @if ($locale == 'en')
                        {!! setting('site.company_description_en')  !!}
                    @else
                        {!! setting('site.company_description')  !!}
                    @endif
                </div>
                {{--<div class="large-6 small-12 info">
                    <span class="text">
                        {{ __('message.about_des1') }}

                    </span>
                    <span class="text bold">
                        TOPVN GROUP
                    </span>
                    <span class="text">
                         {{ __('message.about_des2') }}
                    </span>
                    <br />
                    <br />
                    <span class="text">
                         {{ __('message.about_des3') }}
                    </span>
                    <span class="text bold">
                        TOPVN GROUP
                    </span>
                    <span class="text">
                         {{ __('message.about_des4') }}
                    </span>
                    <br />
                    <br />
                    <span class="text">
                        {{ __('message.about_des5') }}
                    </span>
                    <span class="text bold">
                        TOPVN GROUP
                    </span>
                    <span class="text">
                        {{ __('message.about_des6') }}
                    </span>
                    <br />
                    <br />
                    <span class="text bold">
                        TOPVN GROUP
                    </span>
                    <span class="text">
                         {{ __('message.about_des7') }}
                    </span>
                </div>--}}
                <div class="grid-x button-container">
                    @foreach($arrInfo as $itemInfo)
                        <div class="large-3 medium-6 small-12 item">
                            <div class=" button orange grid-x">
                                <div class="small-12">{{ $itemInfo  }}</div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="description-container" style="width: 100%">
                    <div class="info">
                        @if ($locale == 'en')
                            {!! setting('site.company_info_description_en')  !!}
                        @else
                            {!! setting('site.company_info_description')  !!}
                        @endif
                        {{--<span class="text bold">
                            TOPVN GROUP
                        </span>
                        <span class="text">
                            {{ __('message.about_sub_des1') }}
                        </span>
                        <br />
                        <br />
                        <span class="text"> {{ __('message.about_sub_des2') }}</span>
                        <span class="text bold">
                            TOPVN GROUP
                        </span>
                        <span class="text">
                             {{ __('message.about_sub_des3') }}
                        </span>
                        <br />
                        <br />
                        <span class="text bold">
                            TOPVN GROUP
                        </span>
                        <span class="text">
                             {{ __('message.about_sub_des4') }}
                        </span>
                        <br/>
                        <br/>
                        <span class="text">
                             {{ __('message.about_sub_des5') }}
                        </span>
                        <span class="text bold">
                            TOPVN GROUP,
                        </span>
                        <span>
                             {{ __('message.about_sub_des6') }}
                        </span>--}}
                    </div>
                </div>

            </div>

        </div>
        <div class="small-12 grid-x detail-container">
            <div class="large-10 large-offset-1 grid-x content grid-content">
                @foreach($arrMissionData as $itemMission)
                    <div class="large-4 item">
                        <div class="title-container">
                            <div class="title">
                                {!! $itemMission['title'] !!}
                            </div>
                        </div>
                        <div class="description">
                            <span class="text">
                                {!! $itemMission['text'] !!}
                            </span>
                        </div>
                    </div>
                @endforeach

                {{--<div class="large-4 item">
                    <div class="title-container">
                        <div class="title">
                            {{ __('message.about_vision') }}
                        </div>
                    </div>
                    <div class="description">
                        <span class="text">  {{ __('message.about_vision1') }} </span>
                        <span class="text bold">
                            {{ __('message.about_vision2') }}
                        </span>
                        <span class="text">
                              {{ __('message.about_vision3') }}
                        </span>
                    </div>
                </div>
                <div class="large-4 item">
                    <div class="title-container">
                        <div class="title">
                            {{ __('message.about_value') }}
                        </div>
                    </div>
                    <div class="description">
                        <span class="text bold">
                             {{ __('message.about_value1') }}
                        </span>
                        <span class="text">
                             {{ __('message.about_value2') }}
                        </span>
                        <br/>
                        <br/>
                        <span class="text bold">
                           TOPVN GROUP
                        </span>
                        <span class="text">
                             {{ __('message.about_value3') }}
                        </span>
                        <br />
                        <br />
                        <span class="text">
                            {{ __('message.about_value4') }}
                        </span>

                    </div>
                </div>--}}
            </div>
        </div>
        <div class="large-10 large-offset-1 grid-content">
            <div class="staff-container">
                <div class="title">{{ __('message.about_staff') }}</div>
                <div class="subtitle">
                    @if ($locale == 'en')
                        {!! setting('site.company_staff_en')  !!}
                    @else
                        {!! setting('site.company_staff')  !!}
                    @endif
                </div>
                <div class="grid-x staff-list">

                        @foreach($staffs as $staff)
                            <div class="large-3 medium-6 small-12 item">
                                <img class="avatar" src="{{ Voyager::image($staff->avatar)  }}" />
                                <div class="info">
                                    <div class="name">
                                        @if ($locale == 'en' && $staff->name_en)
                                            {{ $staff->name_en }}
                                        @else
                                            {{ $staff->name }}
                                        @endif
                                    </div>
                                    <div class="staff-title">
                                        @if ($locale == 'en' && $staff->title_en)
                                            {{ $staff->title_en }}
                                        @else
                                            {{ $staff->title }}
                                        @endif
                                    </div>
                                    <div class="social-container">
                                        <img src="{{ asset('frontend/img/facebook.svg') }}">
                                        <span>{{ $staff->facebook }}</span>
                                    </div>
                                    <div class="description">
                                        @if ($locale == 'en' && $staff->description_en)
                                            {{ $staff->description_en }}
                                        @else
                                            {{ $staff->description }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach

                </div>
                <?php $lg = Voyager::setting('site.company_image', ''); ?>
                <img class="img-group" src="{{ Voyager::image($lg) }}" />
            </div>
        </div>
    </div>
@stop
