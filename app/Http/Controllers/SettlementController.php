<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Category;
use App\Models\News;
use App\Models\Settlement;
use App\Models\Step;

class SettlementController extends Controller
{
    public function detail(Request $request, $slug) {
       $settlement = Settlement::whereHas('category', function ($q) use($slug) {
           $q->where("slug", $slug);
       })->get()->first();
       $news = News::whereHas('category', function ($q) use($slug) {
           $q->where("slug", $slug);
       })->where('is_published', 1)->where('subcategory', 'SETTLEMENT')->get();
        $steps = Step::whereHas('category', function ($q) use($slug) {
            $q->where("slug", $slug);
        })->get();
        $events = News::whereHas('category', function ($q) use($slug) {
            $q->where("slug", $slug);
        })->where('is_published', 1)->where('subcategory', 'INVESTMENT')->get();
        $categories = Category::Where('slug', '<>', $slug)->has('settlements')->get();
        $settlementChoice = null;
        if (count($categories) > 0) {
            $settlementChoice = $categories[0]->settlements[0];
        }
        $arrayCompare = [];
        $arrayTitle = [];
        if ($settlement && $settlementChoice) {
            $arrayTitle = [$settlement->category->name, $settlementChoice->category->name];
        }
        if ($settlementChoice && $settlement) {
            $arrayIcon = [];
            if ($settlement) {
                $arrayIcon = $settlement->icons->toArray();
            }
            $arrayIconChoice = $settlementChoice->icons->toArray();
            for($i = 0; $i < count($settlement->icons); $i++ ) {
                $index = array_search($settlement->icons[$i]->icon_id, array_column($arrayIconChoice, 'icon_id'));
                $value2 = '';
                if ($index !== false) {
                    $value2 = $arrayIconChoice[$index]['value'];
                }
                $arrayCompare[] = [
                    "icon_id" => $settlement->icons[$i]->icon_id,
                    "icon" => $settlement->icons[$i]->icon->image_black,
                    "value1" => $settlement->icons[$i]->value,
                    "value2" => $value2
                ];
            }
            for($i = 0; $i < count($settlementChoice->icons); $i++ ) {
                $index = array_search($settlementChoice->icons[$i]->icon_id, array_column($arrayCompare, 'icon_id'));
                if ($index === false) {
                    $arrayCompare[] = [
                        "icon_id" => $settlementChoice->icons[$i]->icon_id,
                        "icon" => $settlementChoice->icons[$i]->icon->image_black,
                        "value1" => "",
                        "value2" => $settlementChoice->icons[$i]->value
                    ];
                }

            }
        }
        $category_all = Category::where('is_hide', 0)->get();
        return view('settlement', compact('category_all','events', 'categories', 'settlement', 'news', 'steps', 'arrayTitle', 'arrayCompare'));
    }

    public function compare(Request $request)
    {
        $categories = Category::Where('slug', '<>', $request->slug)->has('settlements')->get();
        $slug = $request->categoryId;
        $settlement = Settlement::where('id', $request->settlementId)->get()->first();
        $settlementChoice = Settlement::whereHas('category', function ($q) use($slug) {
            $q->where("id", $slug);
        })->get()->first();
        $arrayCompare = [];
        $arrayTitle = [$settlement->category->name, $settlementChoice->category->name];
        if ($settlementChoice) {
            $arrayIcon = [];
            if ($settlement) {
                $arrayIcon = $settlement->icons->toArray();
            }

            $arrayIconChoice = $settlementChoice->icons->toArray();
            for($i = 0; $i < count($settlement->icons); $i++ ) {
                $index = array_search($settlement->icons[$i]->icon_id, array_column($arrayIconChoice, 'icon_id'));
                $value2 = '';
                if ($index !== false) {
                    $value2 = $arrayIconChoice[$index]['value'];
                }
                $arrayCompare[] = [
                    "icon_id" => $settlement->icons[$i]->icon_id,
                    "icon" => $settlement->icons[$i]->icon->image_black,
                    "value1" => $settlement->icons[$i]->value,
                    "value2" => $value2
                ];
            }
            for($i = 0; $i < count($settlementChoice->icons); $i++ ) {
                $index = array_search($settlementChoice->icons[$i]->icon_id, array_column($arrayCompare, 'icon_id'));
                if ($index === false) {
                    $arrayCompare[] = [
                        "icon_id" => $settlementChoice->icons[$i]->icon_id,
                        "icon" => $settlementChoice->icons[$i]->icon->image_black,
                        "value1" => "",
                        "value2" => $settlementChoice->icons[$i]->value
                    ];
                }

            }
        }
        $categoryId = $request->categoryId;
        $category_all = Category::where('is_hide', 0)->get();
        return view('compare', compact('category_all', 'categories', 'settlement', 'arrayTitle', 'arrayCompare', 'categoryId'))->render();
    }
}
