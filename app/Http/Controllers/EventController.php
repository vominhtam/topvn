<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Category;
use App\Models\Event;
use App\Models\Gallery;

class EventController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::where('page', 'EVENT')->get()->first();
        $events = Event::all();
        $galleries = Gallery::all();
        $categories = Category::where('is_hide', 0)->get();
        return view('event', compact('banner', 'events', 'galleries', 'categories'));
    }

    public function detail($id) {
        $banner = Banner::where('page', 'EVENT')->get()->first();
        $event = Event::where('id', $id)->first();
        $category_id = $event->category_id;
        $galleries = Gallery::where('event_id', $id)->get();
        $categories = Category::where('is_hide', 0)->get();
        return view('event_detail', compact('categories', 'banner', 'event', 'galleries'));
    }

}
