<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Partner;
use App\Models\News;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::where('page', 'HOME')->get();
        $videos = Video::where('is_published', 1)->get();
        $services = Service::all();
        $partners = Partner::all();
        $benefits = Benefit::all();
        $categories = Category::where('is_hide', 0)->get();
        $news = News::with('category')->where('is_published', 1)->limit(4)->get();
        return view('home', compact('categories','banner', 'services', 'benefits', 'videos', 'news', 'partners'));
    }
}
