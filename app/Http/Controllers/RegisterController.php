<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Partner;
use App\Models\News;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CustomerExport;

class RegisterController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::where('page', 'REGISTER')->get()->first();
        $categories = Category::where('is_hide', 0)->get();
        return view('register', compact('categories','banner'));
    }


    public function store(Request $request)
    {
        $customer = new Customer;
        $customer->category_id = $request->category;
        $customer->fullname = $request->fullname;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->save();
        return response()->json(['success'=> true]);
    }

    public function export_csv(Request $request) {
        $slug = 'customers';

        $dataType = \Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app('App\Models\Customer'));

        // Init array of IDs
        $model = app('App\Models\Customer');
        $query = $model::select('*');
        $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), 'get']);
        return Excel::download(new CustomerExport($dataTypeContent, $dataType), 'customers.csv', \Maatwebsite\Excel\Excel::CSV);

    }

}
