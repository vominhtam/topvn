<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Partner;
use App\Models\Education;

class EducationController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::where('page', 'EDUCATION')->get()->first();
        $educationHot = Education::where('type', 'HOT')->get();
        $categories = Category::where('is_hide', 0)->get();
        return view('education', compact('categories','banner', 'educationHot'));
    }

    public function detail(Request $request, $id) {
        $banner = Banner::where('page', 'EDUCATION')->get()->first();
        $news = Education::where('id', '<>', $id);
        $new = Education::where('id', $id)->get()->first();
        $categories = Category::where('is_hide', 0)->get();
        return view('education_detail', compact('categories','new', 'banner', 'news'));
    }
}
