<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Partner;
use App\Models\News;

class RecruitmentController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::where('page', 'RECRUITMENT')->get()->first();
        $news = News::with('category')
            ->where('is_published', 1)
            ->where('subcategory', 'RECRUITMENT')
            ->paginate(2);
        $categories = Category::where('is_hide', 0)->get();
        return view('recruitment', compact('categories','banner', 'news'));
    }
}
