<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Event;
use App\Models\News;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Partner;
use App\Models\Gallery;
use App\Models\GroupGallery;

class GalleryController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        $banner = Banner::where('page', 'IMAGE')->get()->first();
        $galleries = Gallery::paginate(4, ['*'], 'pg');
        $videos = Video::paginate(3, ['*'], 'pv');
        $events = Event::has('gallery')->get();
        $services = Service::has('gallery')->get();
        $groups =  GroupGallery::paginate(4, ['*'], 'pg');
        $categories = Category::where('is_hide', 0)->get();
        return view('gallery', compact('categories','groups', 'events', 'services', 'banner', 'galleries', 'videos', 'news'));
    }
    public function detail(Request $request, $slug) {
        $banner = Banner::where('page', 'IMAGE')->get()->first();
        $galleries = Gallery::whereHas('group_gallery', function ($q) use($slug) {
            $q->where("slug", $slug);
        })->get();
        $categories = Category::where('is_hide', 0)->get();
        return view('gallery_detail', compact('categories','galleries', 'banner'));
    }
}
