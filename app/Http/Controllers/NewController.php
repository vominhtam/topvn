<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Partner;
use App\Models\News;

class NewController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category = '';
        $news1 = News::with( 'category')
            ->where('is_published', 1)
            ->where('subcategory', 'RECRUITMENT')
            ->paginate(5, ['*'], 'p2');
        $banner = Banner::where('page', 'NEWS')->get()->first();
        $news3 = News::all();
        $news = News::with('category')->where('is_published', 1)->paginate(5, ['*'], 'p1');
        $news2 = News::with('category')
            ->where('is_published', 1)
            ->where('subcategory', 'SETTLEMENT')
            ->paginate(5, ['*'], 'p3');
        $categories = Category::where('is_hide', 0)->get();
        return view('new', compact('category', 'categories', 'banner', 'news', 'news2', 'news3', 'news1'));
    }
    public function detail(Request $request, $id) {
        $banner = Banner::where('page', 'NEWS')->get()->first();
        $news = News::all();
        if (!$id) {
            return abort(404);
        }
        if (is_numeric($id)) {
            $new = News::where('id', $id)->get()->first();
        } else{
            $new = News::where('slug', $id)->get()->first();
        }
        $categories = Category::where('is_hide', 0)->get();
        return view('new_detail', compact('categories','new', 'banner', 'news'));
    }
}
