<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Partner;
use App\Models\News;

class ServiceController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::where('page', 'SERVICE')->get()->first();
        $gallery = Gallery::all();
        $new = News::where('id', 10)->get()->first();
        $categories = Category::where('is_hide', 0)->get();
        return view('service', compact('categories','banner', 'new', 'gallery'));
    }
}
