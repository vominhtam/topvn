<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Partner;
use App\Models\Contact;

class ContactController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::where('page', 'CONTACT')->get()->first();
        $categories = Category::where('is_hide', 0)->get();
        return view('contact', compact('banner', 'categories'));
    }

    public function store(Request $request)
    {
        $customer = new Contact;
        $customer->email = $request->email;
        $customer->name = $request->fullname;
        $customer->phone = $request->phone;
        $customer->title = $request->title;
        $customer->content = $request->content;
        $customer->save();
        return response()->json(['success'=> true]);
    }
}
