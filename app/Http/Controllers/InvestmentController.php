<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\HomeContent;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Benefit;
use App\Models\Video;
use App\Models\Partner;
use App\Models\News;

class InvestmentController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id) {
        $banner = Banner::where('page', 'INVESTMENT')->get()->first();
        $news = News::with( 'category')
            ->where('is_published', 1)
            ->where('subcategory', 'INVESTMENT')
            ->whereHas('category', function ($q) use($id) {
                $q->where("slug", $id);
            })
            ->paginate(8, ['*'], 'p');
        $category = Category::where('slug', $id)->get()->first();
        $categories = Category::where('is_hide', 0)->get();
        $categoryName = $category->name;
        return view('investment', compact('categories', 'banner', 'news', 'categoryName'));
    }

    public function detail(Request $request, $id) {
        $banner = Banner::where('page', 'INVESTMENT')->get()->first();
        $news = News::where('subcategory', 'INVESTMENT')->get();
        if (!$id) {
            return abort(404);
        }
        if (is_numeric($id)) {
            $new = News::where('id', $id)->get()->first();
        } else{
            $new = News::where('slug', $id)->get()->first();
        }
        $categories = Category::where('is_hide', 0)->get();
        return view('investment_detail', compact('categories','new', 'banner', 'news'));
    }
}
