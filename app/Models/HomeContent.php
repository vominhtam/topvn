<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Support\Facades\DB;

class HomeContent extends Model
{
    use Translatable;

    protected $translatable = ['content'];

    protected $table = 'home_contents';

    protected $fillable = ['content'];

    public function save(array $options = [])
    {
        if ($this->default) {
            DB::table('home_content')->where('id' , '!=', $this->id)->update(['default' => 0]);
        }
        parent::save();
    }
}
