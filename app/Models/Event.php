<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Builder;

class Event extends Model
{
    use Translatable;

    protected static function boot()
    {
        parent::boot();

        // Order by name ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }

    public function category()
    {
        return $this->belongsTo(Voyager::modelClass('Category'));
    }
    public function gallery()
    {
        return $this->hasMany('App\Models\Gallery');
    }
}
