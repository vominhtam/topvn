<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;

class Customer extends Model
{
    use Translatable;

    public function category()
    {
        return $this->belongsTo(Voyager::modelClass('Category'));
    }
}
