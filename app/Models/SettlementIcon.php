<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;

class SettlementIcon extends Model {
    public function icon()
    {
        return $this->belongsTo('App\Models\Icon');
    }
}
