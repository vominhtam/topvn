<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;


class Gallery extends Model
{
    use Translatable;

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }
    public function group_gallery()
    {
        return $this->belongsTo('App\Models\GroupGallery');
    }
}
