<?php

namespace App\Exports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\FromArray;

class CustomerExport implements FromArray
{
	protected $dataTypeContent;
	protected $dataType;
	protected $fields;
	public function __construct($dataTypeContent,$dataType){
		$this->dataTypeContent = $dataTypeContent;
		$this->dataType = $dataType;
		$this->fields = ["fullname","category","email","phone"];
	}
    /**
    * return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $arrayTitle = $this->get_title();

        $arrayResult = [$arrayTitle];
    	foreach ($this->dataTypeContent as $item) {
    	    $arrayValue = [];
    	    foreach ($this->fields as $index=>$row) {
    	        $s="";
                if ($row === 'category') {

                    $s = $item->{$row} ? $item->{$row}->name : '';
                } else {
                    $s = $item->{$row};
                }
                $arrayValue[] = $s;
            }
            $arrayResult[] =$arrayValue;
        }
        return $arrayResult;
    }

    private function get_title() {
        $arrayTitle = [];
        foreach($this->fields as $i=>$v){
            foreach($this->dataType->readRows as $row){
                if($row->field==$v){
                    $arrayTitle[] = $row->display_name;
                    break;
                }
            }
        }
        return $arrayTitle;
    }
}
